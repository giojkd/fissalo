<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Reservationrow;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

Route::post('/delete-reservation',function(Request $request){
  $data = $request->all();
  $reservationRow = \App\Reservationrow::with(['service'])->find($data['id']);
  if($reservationRow->custom_appointment == 1){
    $reservationRow::unsetEventDispatcher();
  }
  $reservationRow->delete();
});

Route::post('/move-reservation',function(Request $request){
  $data = $request->all();
  $reservationRow = \App\Reservationrow::find($data['id']);

  $reservation = $reservationRow->reservation;

  if($reservation->custom_appointment == 0){
    $service = $reservationRow->service;
    $reservationRow->reservation_begin = $data['reservation_begin'].':00';
    $reservationRow->reservation_end = date('Y-m-d H:i:s',strtotime($data['reservation_begin'].' +'.$service->service_duration.'minutes'));
  }else{
    $duration = round((strtotime($reservationRow->reservation_end)-strtotime($reservationRow->reservation_begin))/60);
    $reservationRow->reservation_begin = $data['reservation_begin'].':00';
    $reservationRow->reservation_end = date('Y-m-d H:i:s',strtotime($data['reservation_begin'].' +'.$duration.'minutes'));
  }

  $reservationRow->save();
});

Route::post('/set-reservation',function(Request $request){

  $data = $request->all();

  if($data['id']>0){
    \App\Reservation::destroy($data['id']);
    \App\Reservationrow::where('reservation_id',$data['id'])->delete();

  }

  $reservationTime = date('Y-m-d H:i:s',strtotime($data['reservation_day'].' '.$data['reservation_time']));
  $schedule = \App\Schedule::find($data['schedule_id']);
  $reservation = new \App\Reservation();
  $reservation->custom_appointment = $data['customAppointment'];
  $reservation->company_id = $schedule->company_id;
  $reservation->reservation_notes = $data['reservation_notes'];

  switch($data['customAppointment']){
    case 1:
    $reservation->save();
    $reservationRow = new \App\Reservationrow();
    $reservationRow::unsetEventDispatcher();
    $reservationRow->reservation_id = $reservation->id;
    $reservationRow->service_id = 1; #fixed service_id at 1 if it's custom appointment
    $reservationRow->schedule_id = $data['schedule_id'];
    $reservationRow->reservation_begin = $reservationTime;
    $reservationRow->reservation_end = $data['reservation_day'].' '.$data['reservation_end']['hh'].':'.$data['reservation_end']['mm'].':'.'00';
    $reservationRow->save();
    $reservationRows = [];
    $reservationRows[] = $reservationRow;
    $return = [
      'reservation' => $reservation,
      'reservation_rows' => $reservationRows,
    ];
    break;
    case 0:
    $client_id = $data['client']['id'];
    if($client_id==0){
      if($data['client']['client_email']!=''){
        $client = \App\Client::where('client_email','LIKE',$data['client']['client_email'])->first();
        if(!(is_null($client))){
          $client_id = $client->id;
        }else{
          $client = new \App\Client();
          $client->client_name = $data['client']['client_name'];
          $client->client_surname = $data['client']['client_surname'];
          $client->client_telephone = $data['client']['client_telephone'];
          $client->client_email = $data['client']['client_email'];
          $client->save();
          $client_id = $client->id;
        }
      }
    }else{
      $client = \App\Client::find($client_id);
    }
    $reservation->client_id = $client_id;
    $reservation->save();
    $reservationRows = [];
    $services = $data['services'];
    $serviceModel = \App\Service::find($services[0]);
    $reservationRowEnd = date('Y-m-d H:i:s',strtotime($reservationTime.' +'.$serviceModel->service_duration.'minutes'));
    if(count($services)>0){
      foreach($services as $service_id){
        $serviceModel = \App\Service::find($service_id);
        $reservationRow = new \App\Reservationrow();
        $reservationRow->reservation_id = $reservation->id;
        $reservationRow->service_id = $service_id;
        $reservationRow->schedule_id = $data['schedule_id'];
        $reservationRow->reservation_begin = $reservationTime;
        $reservationTime = $reservationRowEnd;
        $reservationRowEnd = date('Y-m-d H:i:s',strtotime($reservationRowEnd.' +'.$serviceModel->service_duration.'minutes'));
        $reservationRow->save();
        $reservationRows[] = $reservationRow;
      }
    }
    $return = [
      'reservation' => $reservation,
      'reservation_rows' => $reservationRows,
      'client' => $client,
    ];
    break;
  }


  return $return;
});

Route::get('/schedule-services',function(Request $request){
  return \App\Schedule::find($request->input('schedule_id'))->services;
});



Route::get('/reservation-details',function(Request $request){
  $reservation =
  \App\Reservationrow
  ::withoutGlobalScopes()
  ->with([
    'service'=>function($query){
      $query->withoutGlobalScopes();
    },
    'reservation'=>function($query){
      $query->withoutGlobalScopes();
    },
    'reservation.reservationrows'=>function($query){
      $query->withoutGlobalScopes()->orderBy('reservation_end');
    },
    'reservation.client'=>function($query){
      $query->withoutGlobalScopes();
    },
    'reservation.company'=>function($query){
      $query->withoutGlobalScopes();
    }
    ])->find($request->input('id'));

    $services = [];
    if(!is_null($reservation->reservation->reservationrows)){
      foreach($reservation->reservation->reservationrows as $rr){
        $services[] = $rr->service_id;
      }
    }



    if($reservation->reservation->custom_appointment == 0){
      $firstReservationRow = $reservation->reservation->reservationrows[0];
      $reservationBegin = strtotime($firstReservationRow->reservation_begin);
      $reservationTime = date('H:i',$reservationBegin);
      $reservationDay = date('Y-m-d',$reservationBegin);
      $reservationEnd = strtotime($reservation->reservation->reservationrows[count($reservation->reservation->reservationrows)-1]->reservation_end);
      $return = [
        'customAppointment' => $reservation->reservation->custom_appointment,
        'reservation_end' => [
                              'hh'=>date('H',$reservationEnd),
                              'mm'=>date('i',$reservationEnd)
                            ],
        'reservation_day' => $reservationDay,
        'reservation_time' => $reservationTime,
        'id' => $reservation->reservation->id,
        'client' => ['id'=>$reservation->reservation->client->id],
        'schedule_id' => $firstReservationRow->schedule_id,
        'reservation_total' => $reservation->reservation->reservation_total,
        'services' => $services,
        'reservation_notes' => $reservation->reservation->reservation_notes
      ];
    }else{
      $firstReservationRow = $reservation->reservation->reservationrows[0];
      $reservationBegin = strtotime($firstReservationRow->reservation_begin);
      $reservationTime = date('H:i',$reservationBegin);
      $reservationDay = date('Y-m-d',$reservationBegin);
      $reservationEnd = strtotime($reservation->reservation->reservationrows[0]->reservation_end);
      $return = [
        'customAppointment' => $reservation->reservation->custom_appointment,
        'reservation_end' => [
                              'hh'=>date('H',$reservationEnd),
                              'mm'=>date('i',$reservationEnd)
                            ],
        'reservation_day' => $reservationDay,
        'reservation_time' => $reservationTime,
        'id' => $reservation->reservation->id,
        'client' => ['id'=>0],
        'schedule_id' => $firstReservationRow->schedule_id,
        'services' => $services,
        'reservation_notes' => $reservation->reservation->reservation_notes
      ];
    }


    return $return;
  });

  Route::get('/reservations',function(Request $request){

    $reservations =
    \App\Reservationrow
    ::withoutGlobalScopes()
    ->with([
      'service'=>function($query){
        $query->withoutGlobalScopes();
      },
      'reservation.client'=>function($query){
        $query->withoutGlobalScopes();
      },
      'reservation.company'=>function($query){
        $query->withoutGlobalScopes();
      }
    ])
    ->whereDate('reservation_begin','=',$request->input('day'))
    ->where('schedule_id','=',$request->input('schedule_id'))
    ->get();

    return $reservations;
  });

  Route::get('/business',function(Request $request){
    $business = \App\Company::first();
    return $business;
  });

  Route::get('/all-clients',function(Request $request){
    return \App\Client::all();
  });

  Route::get('/schedules', function (Request $request) {
    return \App\Schedule::get();
  });

  Route::get('/first-call',function(Request $request){
    return [
      'business' => \App\Company::first(),
      'clients' => \App\Client::all(),
      'schedules' => \App\Schedule::get()
    ];
  });
