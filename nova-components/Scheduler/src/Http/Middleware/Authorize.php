<?php

namespace Mgc\Scheduler\Http\Middleware;

use Mgc\Scheduler\Scheduler;

class Authorize
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next)
    {
        return resolve(Scheduler::class)->authorize($request) ? $next($request) : abort(403);
    }
}
