<?php

namespace Mgc\WeekSchedule;

use Laravel\Nova\Fields\Field;

class WeekSchedule extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'week-schedule';
}
