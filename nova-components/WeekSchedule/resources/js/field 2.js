Nova.booting((Vue, router) => {
    Vue.component('index-week-schedule', require('./components/IndexField'));
    Vue.component('detail-week-schedule', require('./components/DetailField'));
    Vue.component('form-week-schedule', require('./components/FormField'));
})
