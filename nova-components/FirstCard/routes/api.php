<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Card API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your card. These routes
| are loaded by the ServiceProvider of your card. You're free to add
| as many additional routes to this file as your card may require.
|
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });

Route::get('/user_status',function(Request $request){
  $user = Auth::user();
  return $user;
});

Route::get('/user',function(Request $request){
  $user = Auth::user();
  if($user->all_ok()){
    $return['status'] = 1;
  }else{
    $return['status'] = 0;
    $return['user']['name'] = $user->name;

    $return['steps']['step_1']['status'] = 0;
    $return['steps']['step_1']['url'] = '/admin/resources/companies/'.$user->company_id.'/edit';
    $return['steps']['step_1']['text'] = '1 - Completa la configurazione del tuo account';
    $return['steps']['step_2']['status'] = 0;
    $return['steps']['step_2']['url'] = '#';
    $return['steps']['step_2']['text'] = '2 - Crea il tuo primo servizio';
    $return['steps']['step_3']['status'] = 0;
    $return['steps']['step_3']['url'] = '#';
    $return['steps']['step_3']['text'] = '3 - Crea la tua prima agenda';

    if($user->step_1_ok()){
      $return['steps']['step_1']['status'] = 1;
      $return['steps']['step_2']['url'] = '/admin/resources/services/new';
    }
    if($user->step_2_ok()){
      $return['steps']['step_2']['status'] = 1;
      $return['steps']['step_3']['url'] = '/admin/resources/schedules/new';
    }
    if($user->step_3_ok()){
      $return['steps']['step_3']['status'] = 1;
    }
  }

  return $return;
});
