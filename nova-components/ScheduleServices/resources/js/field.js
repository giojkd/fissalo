Nova.booting((Vue, router, store) => {
    Vue.component('index-ScheduleServices', require('./components/IndexField'))
    Vue.component('detail-ScheduleServices', require('./components/DetailField'))
    Vue.component('form-ScheduleServices', require('./components/FormField'))
})
