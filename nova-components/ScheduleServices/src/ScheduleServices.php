<?php

namespace Mgc\ScheduleServices;

use Laravel\Nova\Fields\Field;

class ScheduleServices extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'ScheduleServices';
}
