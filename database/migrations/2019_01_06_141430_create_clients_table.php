<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('client_name',100);
            $table->string('client_surname',100);
            $table->string('client_email',100);
            $table->string('client_telephone',100);
            $table->date('client_date_of_birth')->nullable();
            $table->string('client_gender',10)->nullable();
            $table->integer('client_verified')->nullable();
            $table->string('client_verification_code',5)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
