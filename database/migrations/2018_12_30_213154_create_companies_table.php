<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('company_name',100);
            $table->text('company_description')->nullable();
            $table->text('company_description_short')->nullable();
            $table->string('company_logo')->nullable();
            $table->string('company_telephone',25)->nullable();
            $table->string('company_email',100)->nullable();

            $table->string('full_address',100)->nullable();
            $table->string('administrative_area_level_1',100)->nullable();
            $table->string('administrative_area_level_2',100)->nullable();
            $table->string('administrative_area_level_3',100)->nullable();
            $table->string('locality',100)->nullable();
            $table->string('postal_code',100)->nullable();
            $table->string('route',100)->nullable();
            $table->string('street_number',100)->nullable();
            $table->string('country',100)->nullable();
            $table->string('lat',100)->nullable();
            $table->string('lng',100)->nullable();


            $table->integer('receive_service')->default(1);
            $table->integer('delivery_service')->default(0);
            $table->double('receive_radius')->default(5);
            $table->double('delivery_radius')->default(5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
