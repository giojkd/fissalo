<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleRows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedulerows', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('day_of_the_week');
            $table->integer('schedule_id');
            $table->float('available_minutes')->nullable();
            $table->time('opens_at')->nullable();
            $table->time('closes_at')->nullable();
            $table->time('pause_starts_at')->nullable();
            $table->time('pause_ends_at')->nullable();
            $table->integer('status')->nullable();
            $table->integer('ongoing')->nullable();
            
            $table->index(['schedule_id']);
            $table->unique(['schedule_id','day_of_the_week']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedulerows');
    }
}
