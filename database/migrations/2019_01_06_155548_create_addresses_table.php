<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('full_address',100)->nullable();
            $table->string('administrative_area_level_1',100)->nullable();
            $table->string('administrative_area_level_2',100)->nullable();
            $table->string('administrative_area_level_3',100)->nullable();
            $table->string('locality',100)->nullable();
            $table->string('postal_code',100)->nullable();
            $table->string('route',100)->nullable();
            $table->string('street_number',100)->nullable();
            $table->string('country',100)->nullable();
            $table->string('lat',100)->nullable();
            $table->string('lng',100)->nullable();
            $table->unsignedInteger('client_id');
            $table->index(['client_id']);
            $table->text('address_notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
