<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('client_id')->nullable();
            $table->index(['client_id']);

            $table->unsignedInteger('address_id')->nullable();
            $table->index(['address_id']);

            $table->integer('reservation_status')->default(0);
            
            $table->index(['company_id']);
            $table->unsignedInteger('company_id');
            
            $table->date('reservation_day')->nullable();
            
            $table->integer('payment_online')->default(0);
            $table->integer('payment_online_status')->default(0);
            $table->integer('delivery_service')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
