<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinesstypeTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesstype_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('tag_id');
            $table->integer('businesstype_id');
            $table->unique(['tag_id','businesstype_id']);
            //$table->foreign('tags_id')->references('id')->on('tags');
            //$table->foreign('businesstypes_id')->references('id')->on('businesstypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesstype_tag');
    }
}
