window.fbAsyncInit = function() {
    FB.init({
        appId: '323217925027718',
        status: true,
        cookie: true,
        xfbml: true
    });
};

// Load the SDK asynchronously
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

function login_fb() {
    FB.login(function(response) {
        if (response.authResponse) {
            // connected
            FB.api('/me',{fields:'first_name,last_name,gender,birthday, age_range, email'}, function(response) {
                console.log(response);
                var fb_client = {
                  id:0,
                  client_name:response.first_name+' '+response.last_name,
                  client_email:response.email,
                  client_telephone:'',
                  password:'',
                  facebook_id:response.id
                }
                app.client = fb_client;
                app.authClient();
            });
        } else {
            // cancelled
        }
    }, {scope: 'public_profile, email'});
}

function logout() {
    FB.logout(function(response) {
        // user is now logged out
    });
}
