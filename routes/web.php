<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('placeholder');});
Route::get('/it/', function () {return view('main');});
Route::get('/it/business','Magnaccio@signUpBusiness');#business sign up
Route::get('/it/{url}/', 'Ahab@dispatchTraffic'); #/parrucchiere-l-oreal-a-firenze && #/parrucchiere-via-dei-serragli-firenze
Route::get('/it/{category}/{business_friendly_url_name}','Ahab@serveBusiness'); #/parrucchiere/stefano-pavi-via-dei-serragli-firenze

Route::get('/it/grazie',function(){return view('thank-you');});
Route::get('/it/grazie-business',function(){return view('thank-you-business');});
