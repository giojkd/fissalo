<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('services',function(){

  return \App\Service::get();

});

Route::get('search/{tags}/{location?}/{date?}','Sherlock@searchBusiness');
Route::get('get-business-slots/{date}/{businesses}/{count_slots?}','Sherlock@getBusinessSlots');

Route::get('suggest/{keyword?}','Sherlock@suggester');

Route::get('reservation-rows','Agendum@getReservationsRows');

Route::get('get-reservation-data-modal','Agendum@getReservationDataModal');

Route::get('update-reservationrow','Agendum@updateReservationRow');

Route::get('move-reservationrow','Agendum@moveReservation');

Route::get('load-calendar-modal','Agendum@loadModal');
Route::get('load-event-modal','Agendum@loadEventModal');
Route::get('load-client-data','Agendum@loadClientData');
Route::get('load-address','Agendum@loadAddress');
Route::get('load-service-details','Agendum@loadService');

Route::post('update-reservation/{id}','Agendum@updateReservation');

Route::get('get-day-slots-for-service/{day}/{service_id}','Agendum@getDaySlotsForService');
Route::get('get-days-interval-availability-for-service/{service_id}/{day_begin}/{day_end?}','Agendum@getDaysIntervalAvailabilityForService');#<--- new route to get available days within interval

Route::get('get-reservation-id','Magnaccio@getReservationId');

Route::post('set-reservation-row','Magnaccio@setReservationRow');

Route::post('confirm-reservation','Magnaccio@confirmReservation');

Route::post('unset-reservation-row','Magnaccio@unsetReservationRow');

Route::get('load-services/{companyId}','Company@getCompanyServices');

Route::get('load-company-details/{id}','Company@getCompanyDetails');

Route::get('load-companies/{address}/{tag?}','Company@getCompanies');

Route::get('load-reservation-rows/{id}','Magnaccio@loadReservationRows');

Route::get('load-business-types','Company@loadBusinessTypes');

Route::post('business-sign-up','Magnaccio@businessSignUp');

Route::post('auth-client','Magnaccio@authClient');
Route::post('sign-up-client','Magnaccio@signUpClient');
Route::post('update-client','Magnaccio@updateClient');
Route::post('load-client','Magnaccio@loadClient');

Route::post('delete-reservation','Magnaccio@deleteReservation');

Route::get('load-client-get','Magnaccio@loadClient');

Route::post('edit-client-address','Magnaccio@editClientAddress');
Route::post('delete-client-address','Magnaccio@deleteClientAddress');
