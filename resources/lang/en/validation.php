<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute deve essere accettato.',
    'active_url' => ':attribute non è un url valido.',
    'after' => ':attribute deve essere una data successiva a :date.',
    'after_or_equal' => ':attribute deve essere una data uguale o successiva a to :date.',
    'alpha' => ':attribute può contenere solo lettere.',
    'alpha_dash' => ':attribute può contenere solo lettere, numeri, slashes o underscore.',
    'alpha_num' => ':attribute può contenere solo lettere e numeri.',
    'array' => ':attribute deve essere un array.',
    'before' => ':attribute deve essere una data precedente a :date.',
    'before_or_equal' => ':attribute deve essere una data precedente o uguale a to :date.',
    'between' => [
        'numeric' => ':attribute deve essere tra :min e :max.',
        'file' => ':attribute deve essere tra  :min e :max kilobyte.',
        'string' => ':attribute deve essere tra :min e :max caratteri.',
        'array' => ':attribute deve essere tra :min e :max elementi.',
    ],
    'boolean' => ':attribute deve essere vero o falso.',
    'confirmed' => ':attribute la conferma non matcha l\'originale.',
    'date' => ':attribute non è una data valida.',
    'date_equals' => ':attribute deve essere una data uguale a :date.',
    'date_format' => ':attribute non corrisponde al formato :format.',
    'different' => ':attribute e :other devono essere diversi.',
    'digits' => ':attribute deveessere di :digits caratteri.',
    'digits_between' => ':attribute deve essere tra :min e :max caratteri.',
    'dimensions' => ':attribute ha una dimensione non valida.',
    'distinct' => ':attribute ha un valore duplicato.',
    'email' => ' :attribute deve essere un indirizzo email valido.',
    'exists' => 'Il valore selezionato :attribute non è valido.',
    'file' => ':attribute deve essere un file.',
    'filled' => ':attribute deve avere un valore.',
    'gt' => [
        'numeric' => ':attribute deve essere più grande di :value.',
        'file' => ':attribute deve essere più grande di :value kilobytes.',
        'string' => ':attribute deve essere più lungo di :value caratteri.',
        'array' => ':attribute deve avere più di :value elementi.',
    ],
    'gte' => [
        'numeric' => ':attribute deve essere più grande o uguale di :value.',
        'file' => ':attribute deve essere più grande o uguale di :value kilobyte.',
        'string' => ':attribute deve essere più lungo o uguale di :value caratteri.',
        'array' => ':attribute deve avere almeno :value elementi.',
    ],
    'image' => ':attribute deve essere un\'immagine.',
    'in' => 'Il :attribute che hai selezionato non è validot.',
    'in_array' => ':attribute non esiste in :other.',
    'integer' => ':attribute deve essere un intero.',
    'ip' => ':attribute deve essere un indirizzo IP valido.',
    'ipv4' => ':attribute deve essere un indirizzo IPv4 valido.',
    'ipv6' => ':attribute deve essere un indirizzo IPv6 valido.',
    'json' => ':attribute deve essere un file JSON valido.',
    'lt' => [
        'numeric' => ':attribute deve essere minore di :value.',
        'file' => ':attribute deve essere più piccolo di :value kilobyte.',
        'string' => ':attribute must be less than :value characters.',
        'array' => ':attribute deve avere meno di :value elementi.',
    ],
    'lte' => [
        'numeric' => ':attribute deve essere minore o uguale a :value.',
        'file' => ':attribute deve essere uguale o più piccolo di :value kilobyte.',
        'string' => ':attribute deve essere uguale o più corto di :value caratteri.',
        'array' => ':attribute non deve avere più di :value elementi.',
    ],
    'max' => [
        'numeric' => ':attribute deve essere minore di :max.',
        'file' => ':attribute deve essere più piccolo di :max kilobyte.',
        'string' => ':attribute deve essere più corto di :max caratteri.',
        'array' => ':attribute deve avere meno di :max elementi.',
    ],
    'mimes' => ':attribute deve essere un file di tipo: :values.',
    'mimetypes' => ':attribute deve essere un file di tipo :values.',
    'min' => [
        'numeric' => ':attribute deve essere almeno :min.',
        'file' => ':attribute deve essere almeno :min kilobyte.',
        'string' => ':attribute deve essere lungo almeno :min caratteri.',
        'array' => ':attribute deve avere almeno :min elementi.',
    ],
    'not_in' => 'Il :attribute che hai selezionato non è valido.',
    'not_regex' => 'Il formato di :attribute non è valido.',
    'numeric' => ':attribute deve essere un numero.',
    'present' => ':attribute deve essere presente.',
    'regex' => 'Il formato di :attribute non è valido.',
    'required' => ':attribute è obbligatorio.',
    'required_if' => ':attribute è obbligatorio quando :other ha valore :value.',
    'required_unless' => ':attribute è obbligaotrio almeno che :other non sia in :values.',
    'required_with' => ':attribute è obbligatorio quando :values è presente.',
    'required_with_all' => ':attribute è obbligatorio quando :values sono presenti.',
    'required_without' => ':attribute è obbligaotrio quando :values non sono presenti.',
    'required_without_all' => ':attribute è obbligatorio quando nessuno tra :values è presente.',
    'same' => ':attribute e :other devono matchare.',
    'size' => [
        'numeric' => ':attribute deve essere :size.',
        'file' => ':attribute deve essere :size kilobyte.',
        'string' => ':attribute deve essere :size caratteri.',
        'array' => ':attribute deve contenere :size elementi.',
    ],
    'starts_with' => ':attribute deve iniziare con uno dei seguenti :values',
    'string' => ':attribute deve essere una stringa.',
    'timezone' => ':attribute deve essere una zona valida.',
    'unique' => 'Il valore :attribute è già stato utilizzato.',
    'uploaded' => ':attribute ha fallito durante l\'upload.',
    'url' => 'Il formato di :attribute non è valido.',
    'uuid' => ':attribute deve essere un UUID valido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
