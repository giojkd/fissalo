<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La password deve essere almeno 6 caratteri ed uguale alla conferma.',
    'reset' => 'La tua password è stata resettata!',
    'sent' => 'Ti abbiamo inviato una mail con il link per resettare la password!',
    'token' => 'Il token di reset della password non è valido.',
    'user' => "Non riusciamo a trovare nessun utente con questa email.",

];
