<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
  integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet"
  href="https://erp.amtitalia.com/admin/js/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900"
  rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">

  <title>Hello, world!</title>

</head>
<body>


  <div id="app">

    <div class="container-fluid border-bottom">
      <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse row" id="navbarSupportedContent">
        <div class="col-3">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
              <span class="pointer" @click="openAddressSelection">
                <i class="fas fa-city"></i>
                <span id="navbarAddressHolder">{{address.substring(0, 100)}}</span>
              </span>

              <a v-if="address!=''" href="javascript:" @click="unsetCity"><i
                class="far fa-times-circle"></i></a>
              </li>
            </ul>
          </div>
          <div class="col-6 text-center">
            <a class="navbar-brand" href="/it/">
              <img src="/img/logo_fissalo_front.png" alt="">
            </a>
          </div>
          <div class="col-3 text-right">
            <a class="btn btn-fissalo-secondary mr-1 btn-sm">
              BUSINESS
            </a>
            <a class="btn btn-fissalo-default mr-1 btn-sm">
              ACCEDI
            </a>
          </div>
        </div>
      </nav>
    </div>

    <form v-bind:action="searchFormAction" method="GET" autocomplete="off" @submit.prevent="checkAddress">
      <input type="hidden" v-model="lat">
      <input type="hidden" v-model="lng">
      <input type="hidden" v-model="radius">
      <div class="container-fluid text-center bg-light shadow-sm ">
        <div class="row no-gutters pt-2 pb-2">
          <div class="col-3">

          </div>
          <div class="col-md-3">
            <div class="input-group input-group-lg">
              <input @keyup="resetSearch" required id="keywordInput" type="text" v-model="keyword"
              class="form-control border-top-0 border-left-0 border-bottom-0"
              placeholder="Cosa cerchi?" onfocus="this.placeholder=''"
              onblur="this.placeholder='Cosa cerchi?'">
            </div>

          </div>

          <div class="col-md-2">
            <div class="input-group input-group-lg">
              <input id="whenInput" type="text" v-model="when" class="form-control datepicker border-0"
              placeholder="Quando?" onfocus="this.placeholder=''" onblur="this.placeholder='Quando?'">
            </div>
          </div>
          <div class="col-md-1 text-left">
            <button type="submit" class="btn btn-fissalo-super-primary btn-lg btn-block"><i
              class="fa fa-search"></i></button>
            </div>
            <div class="col-1"></div>
          </div>
        </div>
        <div class="container" v-if="tips.length != 0 && tagSelected == false">
          <div class="row no-gutters">
            <div class="col-3">

            </div>
            <div class="col-6 bg-white shadow-sm">
              <div class="row">
                <div class="col-4">
                  <div class="pl-2">
                    <h5 class="mt-2">Categorie</h5>
                    <ul class="list list-unstyled" v-if="tips.tags.length > 0">
                      <li v-for="tag in tips.tags">
                        <a class="autocompleteItem" href="javascript:"
                        @click="autocompleteTag(tag.tag,'tag')">
                        {{tag.tag}}
                      </a>
                    </li>
                  </ul>
                </div>

              </div>
              <div class="col-4">
                <div class="pl-2">
                  <h5 class="mt-2">Professionisti</h5>
                  <ul class="list list-unstyled" v-if="tips.companies.length > 0">
                    <li v-for="company in tips.companies">
                      <a class="autocompleteItem" v-bind:href="companyUrl(company)">
                        {{company.company_name}}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-4">
                <div class="pl-2">
                  <h5 class="mt-2">Brands</h5>
                  <ul class="list list-unstyled" v-if="tips.brands.length > 0">
                    <li v-for="brand in tips.brands">
                      <a class="autocompleteItem" v-bind:href="brandUrl(brand)">
                        {{brand.brand_name}}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>


    <div class="carousel">
      <div class="owl-carousel owl-theme">
        <img v-for="slide in slides" v-bind:src="slide.src">
      </div>
      <div class="header-info">
        <div class="container">
          <div class="row">
            <div class="col-md-1-12">
              <div class="header-info-content">
                <h1>{{company_name}}</h1>
                <h2><i class="fa fa-location-arrow"></i> {{address}}</h2>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="container  sticky-top mb-4">
      <div class="row">
        <div class="col-md-9">
          <nav class="navbar navbar-expand navbar-lightb">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="#"></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Servizi</a>
              </li>
              <li class="nav-item">
                <form class="form-inline">
                  <input v-on:keyup="filterServices" v-model="serviceSearchKeyword"
                  class="form-control mr-sm-2" type="search" placeholder="Cerca un trattamento"
                  aria-label="Search">
                </form>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Categorie
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#"
                v-for="category in categories">{{category.label}}</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <div class="col-md-3">
      <div class="reservation-box">

      </div>
    </div>
  </div>
</div>

<div class="container ">
  <form id="reservationForm" @submit="submitReservation">
    <div class="row">
      <div class="col-md-9">
        <div class="service-category " v-for="service_category in services">

          <span class="service-category-title">{{service_category.category_label}}</span>
          <div class="service row" v-if="service.show"
          v-for="service in service_category.category_services">
          <div class="col-md-10">
            <h2 class="d-inline service-title">{{service.title}}</h2> {{service.duration}}m
            <p class="service-description">
              {{service.description}}
            </p>
          </div>
          <div class="col-md-2 text-right">
            &euro; {{service.price}}

            <div class="pretty p-icon p-round p-smooth">
              <input @change="selectService" type="checkbox" name="" v-bind:value="service.id"
              v-model="checked_services">
              <div class="state p-success">
                <i class="icon mdi mdi-check"></i>
                <label></label>
              </div>
            </div>


          </div>

        </div>

      </div>
    </div>
    <div class="col-md-3 ">
      <div class="shadow-lg">


        <div v-if="selectedServices.length>0">
          <div class="selected-service" v-for="s in selectedServices">
            <div class="row">
              <div class="col">
                <b>{{s.service.service_name}}</b><br>
                <i>con {{s.schedule.schedule_name}}</i>
              </div>
            </div>
            <div class="row">
              <div class="col-9">
                {{formatDate(s.reservation_begin)}}
              </div>
              <div class="col-3 text-right">
                &euro; {{s.service_price}}
              </div>
            </div>
            <a class="text-danger" href="javascript:" @click="removeService(s.service_id)"><i
              class="far fa-times-circle"></i> Rimuovi </a>
            </div>

          </div>
          <div v-else>
            <div class="alert alert-secondary" role="alert">
              Seleziona dei servizi prima di prenotare
            </div>
          </div>
          <a href="javascript:" @click="confirmReservation" class="btn btn-success btn-lg btn-block">Prenota ora</a>
        </div>
      </div>
    </div>
  </form>
</div>


<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <h6>AZIENDA</h6>
        <ul class="list-unstyled">
          <li>Chi siamo</li>
          <li>Lavora con Noi</li>
          <li>Stampa</li>
          <li>Blog</li>
          <li>Contattaci</li>
        </ul>
      </div>
      <div class="col-md-2">
        <h6>HAI UN'ATTIVITA'?</h6>
        <ul class="list-unstyled">
          <li>Vantaggi</li>
          <li>Quanto Costa</li>
          <li>Proponi Attività</li>
        </ul>
      </div>
      <div class="col-md-3">

      </div>
      <div class="col-md-5">
        <h6>NEWSLETTER</h6>
        <div class="mb-4">
          <form id="newsletterSubscriptionForm" action="index.html" method="post">
            <div class="input-group">

              <input type="email" class="form-control" placeholder="La tua email">
              <div class="input-group-append">
                <button type="submit" class="btn btn-outline-secondary" type="button">Invia</button>
              </div>

            </div>
            <p class="help-block mt-2 text-small">Registrati inserendo il tuo indirizzo e-mail qui sotto
              per ricevere aggiornamenti.<br>Ci impegniamo a non inviare spam.</p>
            </form>
          </div>

          <ul class="list list-inline mt-4">
            <li class="list-inline-item">
              <a href="#"><i class="fab fa-2x fa-facebook-square mr-2"></i></a>
              <a href="#"><i class="fab fa-2x fa-instagram"></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 text-white text-small">
          © 2019 Fissalo.it. All Right Are Reserved. P.Iva: IT012345566343
        </div>
        <div class="col-md-6 text-right">
          <ul class="list list-inline">
            <li class="list-inline-item text-small">
              <a href="#">Condizioni d'uso</a>
            </li>
            <li class="list-inline-item text-small">
              <a href="#">Privacy</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="addressSelection" v-bind:class="{'d-none':hideAddressSelection}">
    <a href="javascript:" @click="closeAddressSelection" id="closeAddressSelectionBtn">X</a>
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1><i class="fas fa-map-marker-alt"></i> INSERISCI IL TUO INDIRIZZO</h1>
          <input type="text" placeholder="Via Roma, 1 5012 Firenze" id="placeAutocomplete">
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal fade" id="selectServiceDateTimeModal" tabindex="-1" role="dialog"
  aria-labelledby="selectServiceDateTimeModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
        <div class="text-center">


          <h4>{{capitalizeFirstLetter(calendar.monthsString)}}</h4>
          <div class="row calendarDaysRow">
            <div class="col-md-1">
              <a href="javascript:" @click="subtractWeek">Prima</a>
            </div>
            <div class="col-md-10">
              <div class="calendarDays calendarDaysTop">
                <div v-for="day in weekDays" class="calendarDay">
                  <a href="javascript:" @click="selectDay(dayDate(day))"
                  v-bind:class="{'active':(dayDate(day)===selectedDay)}">
                  {{dayOfTheWeek(day)}}
                </a>
              </div>
              <div class="clearfix"></div>
            </div>
            <hr>
            <div class="calendarDays calendarDaysBottom">
              <div v-for="day in weekDays" class="calendarDay" :class="{notAvailableDay:checkDayAvailability(dayDate(day))}">
                <a href="javascript:" @click="selectDay(dayDate(day))"
                v-bind:class="{'active':(dayDate(day)===selectedDay)}">
                {{dayOfTheMonth(day)}}
              </a>
              <div class="availabilityIndicator" :class="{notAvailableDay:checkDayAvailability(dayDate(day))}">
                <span></span>
              </div>

            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-md-1">
          <a href="javascript:" @click="addWeek">Dopo</a>
        </div>
      </div>

      <div class="row" v-if="availableSchedules.length > 0">
        <div class="col-md-4"></div>
        <div class="col-md-4">
          <select class="form-control" name="" v-model="selectedSchedule">
            <option value="">Con chiunque</option>
            <option v-bind:value="schedule.id" v-for="schedule in availableSchedules">
              {{schedule.schedule_name}}
            </option>
          </select>
        </div>
      </div>
      <div class="row" v-else>
        <div class="col-md-12">
          <div v-if="selectedDay != ''" class="alert alert-secondary">Nessuna agenda disponibile per il giorno che hai
            scelto.
          </div>
          <div v-else class="alert alert-primary">
            Seleziona il giorno che vuoi prenotare!
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <a class="availableSlot btn btn-fissalo-super-primary mt-1 mr-1" href="javascript:"
          @click="selectSlot(slot.time)" v-if="slot.show" v-for="slot in availableSlots">{{slot.time}}</a>
        </div>
      </div>


    </div>


  </div>
  <!--
  <div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary">Save changes</button>
</div>-->
</div>
</div>
</div>

<div class="modal fade" id="confirmReservationModal" tabindex="-1" role="dialog"
aria-labelledby="confirmReservationModal" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Prenota ora</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form @submit.prevent="confirmReservationSubmit">
        <div class="row">
          <div class="col-md-5 col-sm-12">
            <div class="p-4 bg-white">
              <h4>1) I tuoi dati</h4>
              <div class="form-row mb-2">
                <div class="col">
                  <label>Nome<span class="required">*</span></label>
                  <input v-model="client.client_name" name="client[client_name]"
                  type="first_name" class="form-control" placeholder="Nome"
                  required="">
                  <label>Cognome <span class="required">*</span></label>
                  <input v-model="client.client_surname" name="client[client_surname]"
                  type="last-" class="form-control" placeholder="Cognome" required="">
                </div>
              </div>
              <div class="form-row mb-2">
                <div class="col">
                  <label>Email <span class="required">*</span></label>
                  <input v-model="client.client_email" name="client[client_email]"
                  type="email" class="form-control" placeholder="Email" required="">
                </div>
              </div>
              <div class="form-row mb-2">
                <div class="col">
                  <label>Cellulare <span class="required">*</span></label>
                  <input v-model="client.client_telephone" name="client[client_telephone]"
                  type="phone_number" class="form-control" placeholder="Cellulare"
                  required="">
                </div>
              </div>
              <hr>
              <div class="form-row mb-2">
                <div class="col">
                  <label>Note <span class="required">*</span></label>
                  <textarea v-model="reservation_notes" name="" class="form-control" placeholder="Hai delle note per il salone?"></textarea>
                </div>
              </div>
              <!--<div class="form-row mb-2">
              <div class="col">
              <label>Address <span class="required">*</span></label>
              <input id="addressInput" name="address[address]" type="text" class="form-control" placeholder="Address" required="" autocomplete="off">
            </div>
          </div>-->
        </div>
      </div>

      <div class="col-md-6 col-sm-12">
        <div class="p-4 bg-white">
          <h4>2) Pagamento</h4>
          <div class="form-check">
            <input v-model="payment_method" class="form-check-input" type="radio" name="payment_method_id" id="exampleRadios1" value="1" checked>
            <label class="form-check-label" for="exampleRadios1">
              Paga online con carta di credito
            </label>
          </div>
          <div class="form-check">
            <input v-model="payment_method" class="form-check-input" type="radio" name="payment_method_id" id="exampleRadios2" value="2">
            <label class="form-check-label" for="exampleRadios2">
              Paga in negozio
            </label>
          </div>
          <hr>
          <div class="paymentMethodInfo" id="paymentMethodInfo-1">
            <div class="form-row mb-2">
              <div class="col">
                <label>Numero della carta di credito <span class="required">*</span></label>
                <input v-model="cc.num" name="card[num]" type="text"
                class="form-control isRequired"
                placeholder="ex. 1234 1234 1234 1234" required>
              </div>
            </div>
            <div class="form-row mb-2">
              <div class="col">
                <label>Nome del titolare <span
                  class="required">*</span></label>
                  <input v-model="cc.name" name="card[name]" type="text"
                  class="form-control isRequired"
                  placeholder="Nome del titolare" required>
                </div>
              </div>
              <div class="form-row mb-2">
                <div class="col">
                  <label>Data di scadenza <span
                    class="required">*</span></label>
                    <div class="form-row">
                      <div class="col"><input v-model="cc.mm" name="card[mm]"
                        type="text"
                        class="form-control isRequired"
                        placeholder="MM"
                        required></div>
                        <div class="col"><input v-model="cc.yy" name="card[yy]"
                          type="text"
                          class="form-control isRequired"
                          placeholder="AA"
                          required></div>
                        </div>
                      </div>
                    </div>
                    <div class="form-row mb-2">
                      <div class="col">
                        <label>Codice di sicurezza <span
                          class="required">*</span></label>
                          <input v-model="cc.ccv" name="card[ccv]" type="text"
                          class="form-control isRequired"
                          placeholder="• • •" required>
                        </div>
                      </div>
                    </div>
                    <div class="paymentMethodInfo" id="paymentMethodInfo-2">
                      Pagherai per i servizi acquistati in negozio. <br>
                      Ricorda di portare con te un metodo di pagamento tra quelli accettati dal negozio.
                    </div>
                  </div>


                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-danger" role="alert" v-if="ccErrors!=''">
                    {{ccErrors}}
                  </div>
                  <button type="submit" class="btn btn-success btn-lg btn-block" v-bind:disabled="sendingCcRequest">Conferma e prenota
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
  integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
  integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
  crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
  integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
  crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.5.22/dist/vue.js"></script>

  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script type="text/javascript"
  src="https://maps.googleapis.com/maps/api/js?key=<?=env('ADDRESS_AUTOCOMPLETE_API_KEY')?>&libraries=places,drawing&sensor=true&types=establishment"></script>
  <script type="text/javascript"
  src="https://erp.amtitalia.com/admin/js/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js"></script>
  <link rel="stylesheet" href="https://www.mamaflorence.com/static/js/owl/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="https://www.mamaflorence.com/static/js/owl/dist/assets/owl.theme.default.min.css">
  <script type="text/javascript" src="https://www.mamaflorence.com/static/js/owl/dist/owl.carousel.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/it.js"></script>
  <script type="text/javascript" src="/js/facebook.js" ></script>
  <script>


  $(function () {
    $('.datepicker').datepicker();
    $('input[type="radio"][name="payment_method_id"]').change(function(){

      $('.paymentMethodInfo').hide();
      $('.paymentMethodInfo input').removeAttr('required');
      $('#paymentMethodInfo-'+$(this).val()).show();
      $('#paymentMethodInfo-'+$(this).val()+' .isRequired').attr('required','required');
      //<div class="paymentMethodInfo" id="paymentMethodInfo-1">
    })

  })

  var companyId = <?php echo $company_id; ?>;

  var app = new Vue({
    el: '#app',

    methods: {
      checkDayAvailability:function(day){
        return !this.weekDaysAvailability[day];
      },
      checkAddress(){
        this.isSubmit = true;
        if(this.address == ''){
          this.openAddressSelection();
          $('#keywordInput,#whenInput').blur();
          $('#placeAutocomplete').trigger('click');
        }else{
          this.goToResults();
        }
      },
      slugify(string) {
        const a = 'àáäâãåăæçèéëêǵḧìíïîḿńǹñòóöôœṕŕßśșțùúüûǘẃẍÿź·/_,:;'
        const b = 'aaaaaaaaceeeeghiiiimnnnoooooprssstuuuuuwxyz------'
        const p = new RegExp(a.split('').join('|'), 'g')
        return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with ‘and’
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
      },
      companyUrl: function (company) {
        var url = [];
        url.push(this.slugify(company.businesstype.business_type_name));
        url.push(this.slugify(company.company_name) + '-' + this.slugify(company.locality + ' ' + company.route));
        return '/it/' + url.join('/');
      },
      brandUrl: function (brand) {
        var url = [];
        url.push(this.slugify(brand.businesstype.business_type_name));
        url.push(this.slugify(brand.brand_name));
        url.push('a');
        url.push(this.slugify(this.address));
        return '/it/' + url.join('-');
      },
      tagUrl: function (tag) {
        var url = [];
        url.push(this.slugify(tag));
        url.push(this.slugify(this.address));
        return '/it/' + url.join('-');
      },
      goToResults() {
        switch (this.searchMode) {
          case 'tag':
          this.searchFormAction = this.tagUrl(this.keyword);
          break;
        }
        location.href = this.searchFormAction
      },
      capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      },
      formatDate(date) {
        return moment(date).format('lll')
      },
      unsetCity() {
        localStorage.removeItem('fissaloAddress');
        this.address = '';
      },
      resetSearch: function () {
        this.tagSelected = false;
      },
      confirmReservationSubmit: function () {
        var postData = {
          client: this.client,
          cc: this.cc,
          payment_method: this.payment_method,
          reservation_id:this.reservationId,
          notes:this.reservation_notes,
          company_id:companyId
        };
        this.sendingCcRequest = true;
        this.ccErros = '';
        axios
        .post('/api/confirm-reservation/', postData)
        .then(response => {
          console.log(response.data);
          this.sendingCcRequest = false;

          app.ccErrors = response.data.errors;
          switch(response.data.status){
            case 0:
            this.sendingCcRequest = false;
            break;
            case 1:
            localStorage.removeItem("reservation_id")

            /*
            if(response.data.new_reservation_id > 0){
            localStorage.setItem("reservation_id",response.data.new_reservation_id);
            app.reservationId = response.data.new_reservation_id;
            app.selectedServices = [];
            app.checked_services = [];
          }
          */

          window.location.replace('/it/grazie');
          break;
        }
      })
    },
    confirmReservation: function () {
      if (this.selectedServices.length > 0) {
        $('#confirmReservationModal').modal('show');
      } else {
        alert('Seleziona dei servizi prima di prenotare')
      }

    },
    loadServices: function () {
      axios
      .get('/api/load-services/' + companyId)
      .then(response => {
        this.services = response.data
      })
    },
    filterServices: function () {
      for (var i in this.services) {
        for (var j in this.services[i].category_services) {
          if (this.services[i].category_services[j].title.toLowerCase().indexOf(this.serviceSearchKeyword.toLowerCase()) !== -1) {
            this.services[i].category_services[j].show = true;
          } else {
            this.services[i].category_services[j].show = false;
          }
        }
      }
    },
    getReservationId: function () {
      //alert('gettin reservationId');
      var reservationId = localStorage.getItem("reservation_id");
      var rri;
      if (typeof (reservationId) != 'undefined' && reservationId > 0) {
        if (parseInt(reservationId) > 0) {
          rri = reservationId;
          axios.get('/api/load-reservation-rows/' + rri)
          .then(response => {
            this.selectedServices = response.data;

            if (this.selectedServices.length > 0) {
              for (var i in this.selectedServices) {
                var cs = this.selectedServices[i];
                this.checked_services.push(cs.service_id);
              }
            }
          })
        }
      } else {
        axios.get('/api/get-reservation-id')
        .then(response => {
          localStorage.setItem("reservation_id", response.data.id);
          rri = response.data.id;
          app.reservationId = response.data.id;
        })
        this.selectedServices = [];
      }


      return rri;
    },
    selectSlot: function (time) {
      this.selectedSlot = time;
      $('#selectServiceDateTimeModal').modal('hide');
      //selectedDay,selected_service_id,selectedSchedule,selectedSlot

      if (parseInt(this.selectedSchedule) > 0) {
        // now we can notify which schedules has been assigned to his reservation
      } else {
        for (var i in this.availableSlots) {
          var slot = this.availableSlots[i];
          if (slot.time == this.selectedSlot) {
            this.selectedSchedule = slot.schedules[0];
          }
        }
      }

      var reservationRow = {
        day: this.selectedDay,
        time: this.selectedSlot,
        service_id: this.selected_service_id,
        schedule_id: this.selectedSchedule,
        reservation_id: this.reservationId
      }

      console.log('resevationRow');
      console.log(reservationRow);

      axios
      .post('/api/set-reservation-row', reservationRow)
      .then(response => {
        this.selectedServices.push(response.data.reservationRow);
      })


    },
    filterSlots: function (scheduleId) {
      if (scheduleId != '') {
        if (this.availableSlots.length > 0) {
          for (var i in this.availableSlots) {
            if (this.availableSlots[i].schedules.includes(scheduleId)) {
              this.availableSlots[i].show = true;
            } else {
              this.availableSlots[i].show = false;
            }
          }
        }
      } else {
        for (var i in this.availableSlots) {
          this.availableSlots[i].show = true;
        }
      }

    },
    selectDay: function (day) {
      this.selectedDay = day;
      axios.get('/api/get-day-slots-for-service/' + day + '/' + this.selected_service_id)
      .then(response => {
        console.log(response.data);
        this.availableSlots = response.data.availableSlots;
        this.availableSchedules = response.data.availableSchedules;
      })
    },
    dayOfTheWeek: function (day) {
      return moment(day).format('dd').substring(0, 1).toUpperCase();
    },
    dayOfTheMonth: function (day) {
      return moment(day).format('D');
    },
    dayDate: function (day) {
      return moment(day).format('YYYYMMDD');
    },
    selectService: function (e) {

      this.availableSchedules = [];
      this.availableSlots = [];
      this.selectedDay = '';
      this.selectedSchedule = '';
      this.selectedSlot = '';

      if (e.target.checked === true) {
        $('#selectServiceDateTimeModal').modal('show');
        this.selected_service_id = e.target.value;
        this.printWeek();
      } else {
        this.removeService(e.target.value);
      }
    },
    openAddressSelection: function () {
      this.hideAddressSelection = false;
    },
    closeAddressSelection: function () {
      this.hideAddressSelection = true;
    },
    autocompleteTag: function (keyword) {
      this.keyword = keyword;
      this.tips = [];
    },
    search: function () {
    },
    submitReservation: function () {
    },
    addWeek: function () {
      this.calendar.firstDay = moment(this.calendar.firstDay).add('days', 7);
      this.printWeek();
    },
    subtractWeek: function () {
      this.calendar.firstDay = moment(this.calendar.firstDay).subtract('days', 7);
      this.printWeek();
    },
    printWeek: function () {

      var returnDays = [];
      for (var i = 0; i <= 7; i++) {
        returnDays.push(moment(this.calendar.firstDay).add('days', i))
      }

      var monthBegin = moment(returnDays[0]).format('MMMM');
      var monthEnd = moment(returnDays[6]).format('MMMM');
      if (monthBegin != monthEnd) {
        this.calendar.monthsString = monthBegin + ' / ' + monthEnd;
      } else {
        this.calendar.monthsString = monthBegin;
      }
      this.weekDaysAvailability = {};
      axios.get('/api/get-days-interval-availability-for-service/'+this.selected_service_id+'/'+returnDays[0].format('YYYYMMDD')+'/'+returnDays[returnDays.length-1].format('YYYYMMDD'))
      .then(response => {
        this.weekDaysAvailability = response.data;
      })

      this.weekDays = returnDays;
      //return returnDays;
    },
    removeService: function (id) {
      this.unselectService(id);
      for (var i in this.selectedServices) {
        var cs = this.selectedServices[i];
        if (cs.service_id == id) {
          this.selectedServices.splice(i, 1)
        }
      }
      axios.post('/api/unset-reservation-row', {
        'reservation_id': this.reservationId,
        'service_id': id
      })
      .then(response => {
        console.log(response.data);
      })

    },
    unselectService: function (id) {

      id = (typeof (id) != 'undefined') ? id : parseInt(this.selected_service_id);
      var arr = Array.from(this.checked_services);
      var index = arr.indexOf(id);
      if (index > -1) {
        this.checked_services.splice(index, 1);
      }
    }
  },
  data: {
    weekDaysAvailability:{},
    weekDays:{},
    reservation_notes:'',
    payment_method:1,
    searchFormAction: '',
    client: {},
    cc: {},
    selectedServices: [], //here on mounted I must load the services that belongs to the reservation
    reservationId: '',
    serviceSearchKeyword: '',
    selectedDay: '',   //selectedDay,selected_service_id,selectedSchedule,selectedSlot
    selectedSchedule: '',
    selectedSlot: '',
    selected_service_id: '', //MUST BE
    selectedServiceName: '',
    availableSlots: [],
    availableSchedules: [],
    ccErrors:'',
    sendingCcRequest:false,
    calendar: {
      firstDay: moment(),
      monthsString: ''
    },
    checked_services: [],
    services: [],
    slides: [
      {
        title: 'bla bla',
        src: 'https://d6e2eexatv3xy.cloudfront.net/venue_images/images/000/021/941/original/hairforce-coverciano-via-federico-tozzi-firenze-specchi.jpg'
      },
      {
        title: 'bla bla',
        src: 'https://d6e2eexatv3xy.cloudfront.net/venue_images/images/000/021/938/original/hairforce-coverciano-via-federico-tozzi-firenze-poltrone.jpg'
      },
      {
        title: 'bla bla',
        src: 'https://d6e2eexatv3xy.cloudfront.net/venue_images/images/000/021/943/original/hairforce-coverciano-via-federico-tozzi-firenze-trattamenti.jpg'
      },
      {
        title: 'bla bla',
        src: 'https://d6e2eexatv3xy.cloudfront.net/venue_images/images/000/021/939/original/hairforce-coverciano-via-federico-tozzi-firenze-ghd.jpg'
      }
    ],
    company_name: 'Stefano Pavi',
    distance: 0.2,
    address: (localStorage.getItem('fissaloAddress')!=null) ? localStorage.getItem('fissaloAddress') : '',
    category: 'parrucchiere',
    categories: [
      {
        id: 1,
        label: 'Colore'
      },
      {
        id: 2,
        label: 'Cura'
      },
      {
        id: 3,
        label: 'Effetti luce'
      },
      {
        id: 4,
        label: 'Taglio'
      },
      {
        id: 5,
        label: 'Piega'
      }
    ],
    keyword: '',
    where: 'Firenze',
    when: '',
    lat: 0,
    lng: 0,
    radius: 5,
    tips: [],
    hideAddressSelection: true
  },

  watch: {
    selectedSchedule: function (newValue, oldValue) {
      this.filterSlots(newValue)
    },
    keyword: function () {
      axios.get('/api/suggest/' + this.keyword)
      .then(response => {
        this.tips = response.data
      })
    }
  },

  mounted: function () {

    ////////////////////////
    //load company details//
    ////////////////////////

    axios
    .get('/api/load-company-details/' + companyId)
    .then(response => {
      console.log(response.data);
      app.company_name = response.data.company_name;
      //app.address = response.data.address;
      app.category = response.data.category;
      app.distance = response.data.distance;
      app.slides = response.data.slides;
      $('.owl-carousel').owlCarousel({
        items: 2,
        dots: false,
        loop: true,
        margin: 4,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        responsive: {
          600: {
            items: 3
          }
        }
      });
    })

    ////////////////////////
    ////////////////////////
    ////////////////////////


    $('#selectServiceDateTimeModal').on('hidden.bs.modal', function (event) {
      if (app.selectedDay == '' || app.selectedSchedule == '' || app.selectedSlot == '') {
        app.unselectService()
      }
    })

    this.reservationId = this.getReservationId();

    this.loadServices();


    var placeAutocomplete = new google.maps.places.Autocomplete(
      (document.getElementById('placeAutocomplete')),
      {
        componentRestrictions: {country: "it"},
        rankBy: google.maps.places.RankBy.DISTANCE
      });

      placeAutocomplete.addListener('place_changed', function () {
        var place = placeAutocomplete.getPlace();
        console.log(place);
        app.address = place.name;
        app.closeAddressSelection();
      })


    }
  })
  </script>
</body>
</html>
