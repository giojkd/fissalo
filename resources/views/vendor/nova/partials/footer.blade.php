
<p class="mt-8 text-center text-xs text-80">
    &copy; {{ date('Y') }} Fissalo.it | All Rights Are Reserved | Made With Love in Firenze
    <span class="px-1">&middot;</span>
</p>


<script src="https://maps.googleapis.com/maps/api/js?key={{config('google-autocomplete.api_key')}}&libraries=places&language=en"></script>


<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />-->

  <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">


<link href="{{ asset('css/mgc.css') }}" rel="stylesheet" />
