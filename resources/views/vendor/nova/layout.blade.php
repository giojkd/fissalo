<!DOCTYPE html>
<html lang="en" class="h-full font-sans antialiased">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=1280">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ Nova::name() }}</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">

  <!-- Styles -->
  <link rel="stylesheet" href="{{ mix('app.css', 'vendor/nova') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <!-- Tool Styles -->
  @foreach(Nova::availableStyles(request()) as $name => $path)
  <link rel="stylesheet" href="/nova-api/styles/{{ $name }}">
  @endforeach
</head>
<body class="min-w-site bg-40 text-black min-h-full">
  <div id="nova">
    <div v-cloak class="flex min-h-screen">

      <!-- Sidebar -->
      <div class="min-h-screen flex-none pt-header min-h-screen w-sidebar bg-40 px-6 text-black">
        <a href="{{ Nova::path() }}">
          <div class="absolute pin-t pin-l pin-r bg-white flex items-center w-sidebar h-header px-6">
            @include('nova::partials.logo')
          </div>
        </a>

        @foreach (Nova::availableTools(request()) as $tool)
        {!! $tool->renderNavigation() !!}
        @endforeach
      </div>

      <!-- Content -->

      <div class="content shadow">



        <div class="flex items-center relative h-header bg-white z-20 px-6">


          <dropdown class="ml-auto h-9 flex items-center dropdown-right">
            @include('nova::partials.user')
          </dropdown>
        </div>


        <div data-testid="content" class="px-view py-view mx-auto">
          @yield('content')

          @include('nova::partials.footer')
        </div>

      </div>

    </div>
  </div>

  <script>
  window.config = @json(Nova::jsonVariables(request()));
</script>

<!-- Scripts -->
<script src="{{ mix('manifest.js', 'vendor/nova') }}"></script>
<script src="{{ mix('vendor.js', 'vendor/nova') }}"></script>
<script src="{{ mix('app.js', 'vendor/nova') }}"></script>

<!-- Build Nova Instance -->
<script>
window.Nova = new CreateNova(config)
</script>

<!-- Tool Scripts -->
@foreach (Nova::availableScripts(request()) as $name => $path)
@if (starts_with($path, ['http://', 'https://']))
<script src="{!! $path !!}"></script>
@else
<script src="/nova-api/scripts/{{ $name }}"></script>
@endif
@endforeach


<!-- Start Nova -->
<script>
Nova.liftOff()
</script>
</body>
</html>
