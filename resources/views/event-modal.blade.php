<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Imposta calendario</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

    	<h4>Cliente</h4>
    	<table class="table-hover table-striped table">
    		<thead>
    			<tr>
    				<th>Nome</th>
    				<th>Cognome</th>
    				<th>Telefono</th>
    			</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>{{$client->client_name}}</td>
    				<td>{{$client->client_surname}}</td>
    				<td>{{$client->client_telephone}}</td>
    			</tr>
    		</tbody>
    	</table>
    	<h4>Servizio</h4>
    	 <div class="row serviceRow" style="margin-bottom:10px;">
                <div class="col-md-3">

                  <select class="form-control" name="reservation_row[schedule_id]">
                    <option value="">Agenda</option>
                    @foreach($available_schedules as $available_schedule)
                      <option @if($available_schedule->id == $reservationrow->schedule_id) selected @endif value="{{$available_schedule->id}}">{{$available_schedule->schedule_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">

                  <select class="form-control serviceSelect" name="reservation_row[service_id]">
                    <option value="">Servizio</option>
                    @foreach($available_services as $available_service)
                    <option data-service_price="{{$available_service->service_price}}" @if($available_service->id == $reservationrow->service_id) selected @endif value="{{$available_service->id}}">{{$available_service->service_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <input class="form-control datetimepicker" type="text" name="reservation_row[reservation_begin]" value="{{date('d/m/Y H:i',strtotime($reservationrow->reservation_begin))}}" placeholder="Quando">
                </div>
                <div class="col-md-3">
                  <div class="input-group">
                    <span class="input-group-addon">€</span>
                    <input class="form-control" type="text" name="reservation_row[service_price]" value="{{$reservationrow->service_price}}">
                  </div>

                </div>
              </div>

	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        <button type="button" class="btn btn-primary">Aggiorna</button>
        <button type="button" class="btn btn-warning">Cancella</button>
      </div>
    </div>
  </div>



  <script type="text/javascript">
    $(function(){
      applyDateTimePicker();
    })
  </script>
