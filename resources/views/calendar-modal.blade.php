
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Imposta calendario</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form class="" action="index.html" method="post" id="reservationEditForm">
        <input type="hidden" name="client[id]" value="{{$client->id}}">
        <h4>Cliente</h4>
        <div class="row">
          <div class="col-md-4">
            <label for="">Cliente</label>
            <select class="form-control" name="reservation[client_id]">
              <option value="">Crea nuovo</option>
              @foreach($available_clients as $available_client)
              <option @if($available_client->id == $client->id) selected @endif value="{{$available_client->id}}">{{$available_client->client_name}} {{$available_client->client_surname}}</option>
              @endforeach

            </select>
          </div>
          <div class="col-md-4">

            <label for="">Nome</label>
            <input type="text" name="client[client_name]" value="{{$client->client_name}}" class="form-control" placeholder="Marco" >
          </div>
          <div class="col-md-4">
            <label for="">Cognome</label>
            <input type="text" name="client[client_surname]" value="{{$client->client_surname}}" class="form-control" placeholder="Rossi">
          </div>
        </div>
        <div class="row" style="margin-top:10px;">
          <div class="col-md-3">
            <label for="">Telefono</label>
            <input type="text" name="client[client_telephone]" value="{{$client->client_telephone}}" class="form-control" placeholder="+393334455566">
          </div>
          <div class="col-md-3">
            <label for="">Email</label>
            <input type="email" name="client[client_email]" value="{{$client->client_email}}" class="form-control" placeholder="marco@rossi.it">
          </div>
          <div class="col-md-3">
            <label for="">Data di nascita</label>
            <input type="text" name="client[client_date_of_birth]" value="{{(strtotime($client->client_date_of_birth)>0) ? date('d/m/Y',strtotime($client->client_date_of_birth)) : ''}}" class="form-control datepicker" placeholder="01/01/1975">
          </div>
          <div class="col-md-3">
            <label for="">Sesso</label>
            <select class="form-control" name="client[client_gender]">
              <option value="woman" @if($client->client_gender == 'woman') selected @endif>Woman</option>
              <option value="man" @if($client->client_gender == 'man') selected @endif>Uomo</option>
            </select>
          </div>
        </div>
        <div class="row" style="margin-top:10px;">
          <div class="col-md-6">
            <label for="">Scegli indirizzo</label>
            <select class="form-control" name="reservation[address_id]">
              <option value="">Crea nuovo<option>
                @foreach($client->addresses as $address)
                  @if($address->id > 0)
                  <option value="{{$address->id}}">
                    {{$address->full_address}}
                  </option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="col-md-6">
              <label for="">Oppure creane uno nuovo</label>
              <input type="text" name="address[full_address]" value="" class="form-control" placeholder="Via Roma, 1 50124 Firenze">
            </div>
          </div>
          <hr>
          <h4>Info</h4>
          <div class="row">
            <div class="col-md-9">
              <textarea name="reservation[reservation_notes]" rows="4" cols="80" class="form-control" placeholder="Note">{{$reservation->reservation_notes}}</textarea>
            </div>
            <div class="col-md-3">
              <label for="">A domicilio</label>
              <input data-delivery_extra="{{$company->delivery_extra}}" type="checkbox" name="reservation[delivery_service]" value="1" @if($reservation->delivery_service == 1) checked @endif>
              <br>
              <label for="">Prezzo</label>
              <div class="input-group">
                <span class="input-group-addon">€</span>
                <input class="form-control" type="text" name="reservation[reservation_notes]" value="{{$reservation->reservation_total}}" placeholder="50,00">
              </div>
            </div>
          </div>
          <hr>
          <h4>Servizi</h4>
          <div class="servicesWrapper">
           
           
            @if(count($rows)>0)
              @foreach($rows as $row)
              <div class="row serviceRow" style="margin-bottom:10px;">
                <div class="col-md-3">
                  <select class="form-control" name="reservation_row[schedule_id]">
                    <option value="">Agenda</option>
                    @foreach($available_schedules as $available_schedule)
                      <option @if($available_schedule->id == $row->schedule_id) selected @endif value="{{$available_schedule->id}}">{{$available_schedule->schedule_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <select class="form-control serviceSelect" name="reservation_row[service_id]">
                    <option value="">Servizio</option>
                    @foreach($available_services as $available_service)
                    <option data-service_price="{{$available_service->service_price}}" @if($available_service->id == $row->service_id) selected @endif value="{{$available_service->id}}">{{$available_service->service_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <input class="form-control datetimepicker" type="text" name="reservation_row[reservation_begin]" value="{{date('d/m/Y H:i',strtotime($row->reservation_begin))}}" placeholder="Quando">
                </div>
                <div class="col-md-3">
                  <a href="javascript:" onclick="$(this).closest('.row').clone().appendTo('.servicesWrapper'); applyDateTimePicker(); calculateReservationPrice()" class="btn btn-primary"><i class="fas fa-plus"></i></a>
                  <a href="javascript:" onclick="if($('.serviceRow').length>1){$(this).closest('.serviceRow').remove(); calculateReservationPrice()}" class="btn btn-default"><i class="fas fa-minus"></i></a>
                </div>
              </div>
              @endforeach
            @else
              <div class="row serviceRow" style="margin-bottom:10px;">
                <div class="col-md-3">
                  <select class="form-control" name="reservation_row[schedule_id]">
                    <option value="">Agenda</option>
                    @foreach($available_schedules as $available_schedule)
                      <option value="{{$available_schedule->id}}">{{$available_schedule->schedule_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <select class="form-control serviceSelect" name="reservation_row[service_id]">
                    <option value="">Servizio</option>
                    @foreach($available_services as $available_service)
                    <option data-service_price="{{$available_service->service_price}}">{{$available_service->service_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <input class="form-control datetimepicker" type="text" name="reservation_row[reservation_begin]" value="" placeholder="Quando">
                </div>
                <div class="col-md-3">
                  <a href="javascript:" onclick="$(this).closest('.row').clone().appendTo('.servicesWrapper'); applyDateTimePicker(); calculateReservationPrice()" class="btn btn-primary"><i class="fas fa-plus"></i></a>
                  <a href="javascript:" onclick="if($('.serviceRow').length>1){$(this).closest('.serviceRow').remove(); calculateReservationPrice()}" class="btn btn-default"><i class="fas fa-minus"></i></a>
                </div>
              </div>
            @endif
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        <button type="button" class="btn btn-primary">Salva</button>
        <button type="button" class="btn btn-warning">Cancella</button>
      </div>
    </div>
  </div>
  <script type="text/javascript">

  
  var reservationEditForm;

  function calculateReservationPrice(){
    var currentTotal = 0;
    $('.serviceSelect').each(function(){
      var selectedOption = $(this).find('option:selected');
      if(selectedOption.attr('value')>0){
          currentTotal+=parseFloat(selectedOption.data('service_price'));
      }
    })
    var delivery = $('input[type="checkbox"][name="reservation[delivery_service]"]');
    if(delivery.is(':checked')){
      currentTotal+=parseFloat(delivery.data('delivery_extra'));
    }
    $('input[name="reservation[reservation_notes]"]').val(currentTotal);
  }

  function checkAddress(){
    var addressSelect = reservationEditForm.find('[name="reservation[address_id]"]');
    var address_id = addressSelect.val();
    if(address_id>0){
      $('[name="address[full_address]"]').attr('disabled',true);
    }else{
      $('[name="address[full_address]"]').attr('disabled',false);
    }
  }

  $(function(){
      reservationEditForm = $('#reservationEditForm');
    $('.serviceSelect, input[type="checkbox"][name="reservation[delivery_service]"]').change(function(){
      calculateReservationPrice();
    })

    applyDateTimePicker();
    checkAddress();


    reservationEditForm.find('[name="reservation[address_id]"]').change(function(){
      checkAddress();
    })
    reservationEditForm.find('[name="reservation[client_id]"]').change(function(){
      var client_id = $(this).val();
      if(parseInt(client_id)>0){
        $.get('/api/load-client-data',{id:client_id},function(r){
          $('[name="client[client_name]"]').val(r.client_name);
          $('[name="client[client_surname]"]').val(r.client_surname);
          $('[name="client[client_telephone]"]').val(r.client_telephone);
          $('[name="client[client_email]"]').val(r.client_email);
          $('[name="client[client_date_of_birth]"]').val(r.client_date_of_birth);
          $('[name="client[client_gender]"]').val(r.client_gender);
        },'json')
      }else{
        $('[name="client[client_name]"]').val('');
        $('[name="client[client_surname]"]').val('');
        $('[name="client[client_telephone]"]').val('');
        $('[name="client[client_email]"]').val('');
        $('[name="client[client_date_of_birth]"]').val('');
        $('[name="client[client_gender]"]').val('man');
      }
    })
  })
  </script>
