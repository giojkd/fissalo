<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/datepicker.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css">
  <!--<link rel="stylesheet" href="/css/webfont/stylesheet.css" type="text/css" charset="utf-8" />-->
  <link rel="stylesheet" href="https://use.typekit.net/vxh1pla.css">
  <title>Fissalo</title>
</head>
<body>


  <div id="app">


    <div class="container-fluid">
      <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse row" id="navbarSupportedContent">
          <div class="col-3">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item " >
                <span class="pointer" @click="openAddressSelection">
                  <i class="fas fa-city"></i>
                  <span id="navbarAddressHolder">{{address.substring(0, 100)}}</span>
                </span>

                <a v-if="address!=''" href="javascript:" @click="unsetCity"><i class="far fa-times-circle"></i></a>
              </li>
            </ul>
          </div>
          <div class="col-6 text-center">
            <a class="navbar-brand" href="/it/">
              <img src="/img/logo_fissalo_front.png" alt="">
            </a>
          </div>
          <div class="col-3 text-right">
            <a class="btn btn-fissalo-secondary mr-1 btn-sm" href="/it/business/">
              BUSINESS
            </a>
            <a v-if="client.id == 0" href="javascript:" class="btn btn-fissalo-default mr-1 btn-sm" data-toggle="modal" data-target="#userModal">
              ACCEDI
            </a>
            <a v-else href="javascript:" class="btn btn-fissalo-default mr-1 btn-sm" data-toggle="modal" data-target="#userModal">
              Ciao {{client.client_name}}
            </a>
          </div>
        </div>
      </nav>
    </div>




    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body">
            <div v-if="client.id==0">

              <div class="p-4 text-center">


                <h2>Accedi</h2>

                <a href="javascript:" onclick="login_fb()" class="btn-facebook"><i class="fab fa-facebook-f"></i> Accedi con Facebook</a>

                <p class="text-center text-muted small">Oppure accedi con Email e Password</p>

                <form @submit.prevent="authClient">
                  <div class="form-group">
                    <input v-model="client.client_email" type="email" placeholder="Email" class="form-control" id="" required>
                  </div>
                  <div class="form-group">
                    <input v-model="client.password" type="password" placeholder="Password" class="form-control" id="" required>
                  </div>
                  <button type="submit" class="btn btn-success btn-block" name="button">Accedi</button>
                </form>

                <p class="text-center text-muted small side-lines"> <span>Oppure se non hai ancora un account </span> </p>
                <h2>Iscriviti</h2>
                <a href="javascript:" onclick="login_fb()" class="btn-facebook"><i class="fab fa-facebook-f"></i> Iscriviti con Facebook</a>
                <p class="text-center text-muted small">Oppure iscriviti con Email e Password</p>
                <form @submit.prevent="signUpClient">
                  <div class="form-group">

                    <input v-model="client.client_name" type="text" placeholder="Nome e Cognome" class="form-control" id="" required>
                  </div>
                  <div class="form-group">
                    <input v-model="client.client_email" type="email" class="form-control" id="" placeholder="Email" required>
                  </div>
                  <div class="form-group">
                    <input v-model="client.password" type="password" class="form-control" id="" placeholder="Password" required>
                  </div>
                  <button type="submit" class="btn btn-success btn-block" name="button">Iscriviti</button>
                </form>

              </div>
            </div>
            <div v-else>
              <h2 class="text-center">Benvenuto {{client.client_name}}!</h2>


              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-reservations-tab" data-toggle="tab" href="#nav-reservations" role="tab" aria-controls="nav-reservations" aria-selected="true">Prenotazioni</a>
                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profilo</a>
                  <a class="nav-item nav-link" id="nav-addresses-tab" data-toggle="tab" href="#nav-addresses" role="tab" aria-controls="nav-profile" aria-selected="false">Indirizzi</a>
                </div>
              </nav>

              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show" id="nav-addresses" role="tabpanel" aria-labelledby="nav-reservations-tab">
                  <h1>I tuoi indirizzi</h1>
                  <div class="d-none mb-2" :class="{'d-block':editingAddress}">

                    <form @submit.prevent="editAddressSubmit">
                      <div class="form-group form-group-lg">
                        <input v-model="currentClientAddress.full_address" type="text" class="form-control" placeholder="Via Roma 1, 50124 Firenze" id="editClientAddressField">
                      </div>
                      <div class="form-group">
                        <textarea placeholder="Hai indicazioni particolari per l'indirizzo?" v-model="currentClientAddress.address_notes" class="form-control"></textarea>
                      </div>
                      <div class="form-group form-group-lg">
                        <input v-model="currentClientAddress.address_label" type="text" class="form-control"  placeholder="Indirizzo di casa">
                      </div>
                      <div class="small text-muted text-center mb-1">
                        <a href="javascript:" @click="editingAddress = 0">Chiudi</a>
                      </div>
                      <button type="submit" class="btn btn-success btn-block" name="button">Salva</button>
                    </form>
                  </div>
                  <a href="javascript:" @click="editAddress(0)" class="btn btn-block btn-primary" :class="{'d-none':editingAddress}"><i class="fa fa-plus"></i> Nuovo indirizzo</a>

                  <div class="mt-2" v-if="clientAddresses.length > 0">
                    <div v-for="clientAddress in clientAddresses" class="card mb-2">
                      <div class="card-body">
                        <div class="row">
                          <div class="col-8">
                            {{clientAddress.full_address}}<br>
                            <span class="small muted">{{clientAddress.address_notes}}</span>
                          </div>
                          <div class="col-4">
                            {{clientAddress.address_label}}
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <a href="javascript:" class="btn btn-outline-secondary btn-sm" @click="editAddress(clientAddress.id)">Modifica </a>
                            <a href="javascript:" class="btn btn-outline-danger btn-sm" @click="deleteAddress(clientAddress.id)">Elimina </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div v-else class="mt-2">
                    <div class="alert alert-secondary" role="alert">
                      Non hai creato nessun indirizzo, crea il tuo primo indirizzo!
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade show active" id="nav-reservations" role="tabpanel" aria-labelledby="nav-reservations-tab">
                  <h1>Le tue prenotazioni</h1>
                  <div class="" v-if="clientReservations.length > 0">
                    <div v-for="reservation in clientReservations" class="card mb-2">
                      <div class="card-body">
                        <div class="row">
                          <div class="col-8">
                            {{printReadableDate(reservation.time.date)}}
                          </div>
                          <div class="col-4">
                            <span class="badge badge-pill" :class="reservation.payment_label_class">{{reservation.payment_label}}</span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-8">
                            <h3>{{reservation.company}}</h3>
                          </div>
                          <div class="col-4">
                            EUR {{reservation.price.toFixed(2)}}
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            {{reservation.service}} con {{reservation.schedule}}
                          </div>
                        </div>
                        <div v-if="reservation.buttons.length>0">
                          <hr>
                          <div class="row">
                            <div class="col" v-for="button in reservation.buttons" v-html="button"></div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="" v-else>

                    <div class="alert alert-secondary" role="alert">
                      Non hai effettuato ancora nessuna prenotazione!
                    </div>
                  </div>

                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                  <h1>Il tuo profilo</h1>
                  <form @submit.prevent="updateClient">
                    <div class="form-group">
                      <input v-model="client.client_email" type="email" class="form-control" id="" placeholder="Email" required disabled>
                    </div>
                    <div class="form-group">
                      <input v-model="client.client_name" type="text" placeholder="Nome e Cognome" class="form-control" id="" required>
                    </div>

                    <div class="form-group">
                      <input v-model="client.client_telephone" type="text" class="form-control" id="" placeholder="Telefono" >
                    </div>
                    <div class="form-group">
                      <input v-model="client.client_date_of_birth" type="text datepicker" class="form-control" id="" placeholder="Data di nascita: 31/12/1970" >
                    </div>
                    <div class="form-group">
                      <input v-model="client.password" type="password" class="form-control" id="" placeholder="Password">
                      <small class="form-text text-muted">
                        Scrivi una password solo se vuoi cambiarla
                      </small>
                    </div>

                    <div class="alert alert-danger" v-if="clientErrors.length > 0">
                      <ul class="list">
                        <li v-for="error in clientErrors">{{error}}</li>
                      </ul>
                    </div>

                    <div class="alert alert-success" v-if="clientSaveStatus==1">
                      Complimenti! Hai aggiornato correttamente i tuoi dati!
                    </div>

                    <button type="submit" class="btn btn-success btn-block" name="button">Aggiorna i tuoi dati</button>
                  </form>
                </div>
              </div>
              <hr>
              <a href="javacript:" class="btn btn-danger btn-block" @click="exitClient()">Esci</a>
            </div>
          </div>


        </div>
      </div>
    </div>

    <div class="bg-light pt-4 pb-4 mb-4">
      <form v-bind:action="searchFormAction" method="GET" autocomplete="off" @submit.prevent="checkAddress">
        <input type="hidden" v-model="lat">
        <input type="hidden" v-model="lng">
        <input type="hidden" v-model="radius">
        <div class="container text-center">
          <div class="row mt-4 mb-4">
            <div class="col-md-12">
              <h1 class="huge">Cerca servizi e professionisti vicino a te</h1>
              <h2 class="h6">In pochi click. Senza costi aggiuntivi.</h2>
            </div>
          </div>
          <div class="row mt-4 no-gutters">
            <div class="col-3">

            </div>
            <div class="col-md-3">
              <div class="input-group input-group-lg">
                <input  @keyup="resetSearch" required id="keywordInput" type="text" v-model="keyword" class="form-control" placeholder="Cosa cerchi?" onfocus="this.placeholder=''" onblur="this.placeholder='Cosa cerchi?'">
              </div>

            </div>

            <div class="col-md-2">
              <div class="input-group input-group-lg">
                <input id="whenInput"type="text" v-model="when" class="form-control datepicker" placeholder="Quando?" onfocus="this.placeholder=''" onblur="this.placeholder='Quando?'">
              </div>
            </div>
            <div class="col-md-1 text-left">
              <button type="submit" class="btn btn-fissalo-super-primary btn-lg btn-block"><i class="fa fa-search"></i></button>
            </div>
            <div class="col-1"></div>
          </div>
        </div>
        <div class="container" v-if="tips.length != 0 && tagSelected == false">
          <div class="row no-gutters">
            <div class="col-3">

            </div>
            <div class="col-6 bg-white shadow-sm">
              <div class="row">
                <div class="col-4">
                  <div class="pl-2">
                    <h5 class="mt-2">Categorie</h5>
                    <ul class="list list-unstyled" v-if="tips.tags.length > 0">
                      <li v-for="tag in tips.tags">
                        <a class="autocompleteItem" href="javascript:" @click="autocompleteTag(tag.tag,'tag')">
                          {{tag.tag}}
                        </a>
                      </li>
                    </ul>
                  </div>

                </div>
                <div class="col-4">
                  <div class="pl-2">
                    <h5 class="mt-2">Professionisti</h5>
                    <ul class="list list-unstyled" v-if="tips.companies.length > 0">
                      <li v-for="company in tips.companies">
                        <a class="autocompleteItem" v-bind:href="companyUrl(company)">
                          {{company.company_name}}
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-4">
                  <div class="pl-2">
                    <h5 class="mt-2">Brands</h5>
                    <ul class="list list-unstyled" v-if="tips.brands.length > 0">
                      <li v-for="brand in tips.brands">
                        <a class="autocompleteItem" v-bind:href="brandUrl(brand)">
                          {{brand.brand_name}}
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

      <div class="container mb-4 mt-4">
        <div class="row">
          <div class="col-2">

          </div>
          <div class="col-8">
            <div class="row">
              <div class="col-md-2 text-center" v-for="i in 6">
                <div class="categoryBlock" >
                  <img src="/img/icons/avvocato.png" alt=""><br>
                  <span>Bellezza e SPA</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="container mt-4 mb-4">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2>Perché usare Fissalo</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 text-center">
          <div class="featureColumn text-center">
            <img src="/img/icons/1.png" alt="" style="width:30%">
            <h3 class="h2">Cerca</h3>
            <h4 class="h6">Tra le migliori attività</h4>
          </div>
        </div>

        <div class="col-md-4 text-center">
          <div class="featureColumn text-center">
            <img src="/img/icons/2.png" alt="" style="width:30%">
            <h3 class="h2">Data & Ora</h3>
            <h4 class="h6">Scegli l'orario</h4>
          </div>
        </div>

        <div class="col-md-4 text-center">
          <div class="featureColumn text-center">
            <img src="/img/icons/3.png" alt="" style="width:30%">
            <h3 class="h2">Prenota</h3>
            <h4 class="h6">Ricevi Reminder Gratuiti</h4>
          </div>
        </div>

      </div>
    </div>
    <div class="container mt-4 mb-4">
      <!--
      <div class="row">
      <div class="col-md-12">
      <h5 class="h2 mb-4">Cerchi qualcos'altro?</h5>
      <a class="tag" v-for="i in 30" href="javascript:" @click="selectTag()">{{i}}</a>
    </div>
  </div>
-->
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="footerHero d-flex justify-content-center" style="background-image:url('/img/footerHeroBackgroundImg.jpg')">
        <div class="align-self-center text-center">
          <h5 class="h4">Restaurateurs join us!</h5>
          <h6>Join more than 40,000 restaurants which fill seats and <br> manage reservations with Fissalo</h6>
          <a href="javascript:" class="btn btn-primary btn-lg">Learn more</a>
        </div>

      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 text-center">
      <span>La tua attività preferita non è ancora su Fissalo?</span> <a href="javascript:">Suggerisci un'attività</a>
    </div>
  </div>
</div>

<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <h6>AZIENDA</h6>
        <ul class="list-unstyled">
          <li>Chi siamo</li>
          <li>Lavora con Noi</li>
          <li>Stampa</li>
          <li>Blog</li>
          <li>Contattaci</li>
        </ul>
      </div>
      <div class="col-md-2">
        <h6>HAI UN'ATTIVITA'?</h6>
        <ul class="list-unstyled">
          <li>Vantaggi</li>
          <li>Quanto Costa</li>
          <li>Proponi Attività</li>
        </ul>
      </div>
      <div class="col-md-3">

      </div>
      <div class="col-md-5">
        <h6>NEWSLETTER</h6>
        <div class="mb-4">
          <form id="newsletterSubscriptionForm" action="index.html" method="post">
            <div class="input-group">

              <input type="email" class="form-control" placeholder="La tua email">
              <div class="input-group-append">
                <button type="submit" class="btn btn-outline-secondary" type="button">Invia</button>
              </div>

            </div>
            <p class="help-block mt-2 text-small">Registrati inserendo il tuo indirizzo e-mail qui sotto per ricevere aggiornamenti.<br>Ci impegniamo a non inviare spam.</p>
          </form>
        </div>

        <ul class="list list-inline mt-4">
          <li class="list-inline-item">
            <a href="#"><i class="fab fa-2x fa-facebook-square mr-2"></i></a>
            <a href="#"><i class="fab fa-2x fa-instagram"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 text-white text-small">
        © 2019 Fissalo.it. All Right Are Reserved. P.Iva: IT012345566343
      </div>
      <div class="col-md-6 text-right">
        <ul class="list list-inline">
          <li class="list-inline-item text-small">
            <a href="#">Condizioni d'uso</a>
          </li>
          <li class="list-inline-item text-small">
            <a href="#">Privacy</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>





<div class="addressSelection" v-bind:class="{'d-none':hideAddressSelection}" >
  <a href="javascript:" @click="closeAddressSelection" id="closeAddressSelectionBtn">X</a>
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="color-fuxia underlined">Cerca vicino a:</h2>
        <ul class="list list-unstyled mainCitiesList">
          <li class="pointer" v-for="mainCity in mainCities" @click="selectAddress(mainCity)">
            {{mainCity.name}}
          </li>
        </ul>
        <input type="text" placeholder="Oppure inserisci indirizzo" onfocus="this.placeholder=''" onblur="this.placeholder='Oppure inserisci indirizzo'" id="placeAutocomplete">

      </div>
    </div>
  </div>
</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.22/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvtm8JBKzp5KzV_vcL2EDpUHWi6omOhVw&libraries=places,drawing&sensor=true&types=establishment"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/it.js"></script>
<script type="text/javascript" src="/js/datepicker.js" ></script>
<script type="text/javascript" src="/js/facebook.js" ></script>
<script type="text/javascript" src="/cache/main_cities.js"></script>
<script>

$(function(){
  $('.datepicker')
  .datepicker({
    format:'dd/mm/yyyy',
    startDate: moment().format('DD/MM/YYYY'),
  })
  .on('changeDate',function(e){
    app.when = moment(e.date).format('DD/MM/YYYY');
    console.log(app.when);
  })
})



var app = new Vue({
  el: '#app',
  methods:{

    unsetCity(){
      localStorage.removeItem('fissaloAddress');
      this.address = '';
    },
    selectAddress(place){
      app.address = place.name;
      localStorage.setItem('fissaloAddress', place.name);
      app.closeAddressSelection();
      if(this.isSubmit){
        this.goToResults();
      }
    },
    goToResults(){
      switch(this.searchMode){
        case 'tag':
        this.searchFormAction = this.tagUrl(this.keyword );
        break;
      }
      location.href = this.searchFormAction
    },
    checkAddress(){
      this.isSubmit = true;
      if(this.address == ''){
        this.openAddressSelection();
        $('#keywordInput,#whenInput').blur();
        $('#placeAutocomplete').trigger('click');
      }else{
        this.goToResults();
      }
    },
    slugify(string) {
      const a = 'àáäâãåăæçèéëêǵḧìíïîḿńǹñòóöôœṕŕßśșțùúüûǘẃẍÿź·/_,:;'
      const b = 'aaaaaaaaceeeeghiiiimnnnoooooprssstuuuuuwxyz------'
      const p = new RegExp(a.split('').join('|'), 'g')
      return string.toString().toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
      .replace(/&/g, '-and-') // Replace & with ‘and’
      .replace(/[^\w\-]+/g, '') // Remove all non-word characters
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, '') // Trim - from end of text
    },
    companyUrl:function(company){
      var url = [];
      url.push(this.slugify(company.businesstype.business_type_name));
      url.push(this.slugify(company.company_name)+'-'+this.slugify(company.locality+' '+company.route));
      return '/it/'+url.join('/');
    },
    brandUrl:function(brand){
      var url = [];
      url.push(this.slugify(brand.businesstype.business_type_name));
      url.push(this.slugify(brand.brand_name));
      url.push('a');
      url.push(this.slugify(this.address));
      return '/it/'+url.join('-');
    },
    tagUrl:function(tag){
      var url = [];
      url.push(this.slugify(tag));
      url.push(this.slugify(this.address));
      console.log(this.when);
      var returnUrl = '/it/'+url.join('-')+'/';
      if(this.when!=''){
        returnUrl+='?il='+moment(this.when,'DD/MM/YYYY').format('YYYY-MM-DD');
      }
      return returnUrl
    },
    openAddressSelection:function(){
      this.hideAddressSelection = false;
    },
    closeAddressSelection:function(){
      this.hideAddressSelection = true;
    },
    autocompleteTag:function(keyword,mode){
      this.keyword = keyword;
      this.tips = [];
      this.searchMode = mode;
      this.tagSelected = true;
    },
    resetSearch:function(){
      this.tagSelected = false;
    },
    search:function(){

    },
    authClient:function(){
      this.client.id = 0;
      this.authStatus = '';
      axios.post('/api/auth-client/',this.client)
      .then(response => {
        console.log(response.data);
        if(response.data.status == 0){
          alert('Login ko')
        }
        if(response.data.status == 1){
          this.client = response.data.client;
          this.authStatus = response.data.status;
          localStorage.setItem('clientId', this.client.id);

        }

      })
    },
    loadClient:function(){
      console.log('loading client');
      this.editingAddress = 0;
      this.clientReservations = [];
      this.client = {};
      axios.post('/api/load-client/',{id:localStorage.getItem('clientId')})
      .then(response => {
        console.log(response.data);
        this.client = response.data.client;
        this.clientReservations = response.data.reservations;
        this.clientAddresses = response.data.addresses;
        this.currentClientAddress.client_id = this.client.id;
      })
    },
    editAddressSubmit:function(){
      this.currentClientAddress.client_id = this.client.id;
      axios.post('/api/edit-client-address/',this.currentClientAddress)
      .then(response => {
        console.log(response.data);
        if(response.data.status == 1){
          this.editingAddress = 0;
          this.loadClient();
        }
      })
    },
    editAddress:function(id){
      this.editingAddress = 1;
      var clientAddressPlaceAutocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('editClientAddressField'))
      );
      clientAddressPlaceAutocomplete.addListener('place_changed', function(innerca) {
        var place = clientAddressPlaceAutocomplete.getPlace();
        app.currentClientAddress.full_address = place.formatted_address;
      })
      if(id>0){
        for (var i in this.clientAddresses){
          if(this.clientAddresses[i].id == id){
            this.currentClientAddress = this.clientAddresses[i];
          }
        }
      }else{
        this.currentClientAddress = {id:0,client_id:0,full_address:'',address_label:'',address_notes:''};
      }
    },
    deleteAddress:function(id){
      if(confirm('Vuoi davvero eliminare l\'indirizzo?')){
        axios.post('/api/delete-client-address/',{id:id})
        .then(response => {
          console.log(response.data);
          this.loadClient();
        })
      }
    },
    signUpClient:function(){
      this.client.id = 0;
      this.authStatus = '';
      axios.post('/api/sign-up-client/',this.client)
      .then(response => {
        this.client = response.data.client;
        this.authStatus = response.data.status;
        localStorage.setItem('clientId', this.client.id);
      })
    },
    updateClient:function(){
      this.clientSaveStatus = 1;
      this.clientErrors = [];
      axios.post('/api/update-client/',this.client)
      .then(response => {
        if(response.data.status == 1){
          this.clientSaveStatus = 1;
          this.loadClient();
        }else {
          this.clientErrors = [];
          this.clientErrors.push(response.data.message)
        }
      })
    },
    deleteReservation:function(id){
      if(confirm('Vuoi davvero cancellare la prenotazione?')){
        axios.post('/api/delete-reservation/',{id:id})
        .then(response => {
          this.loadClient();
        })
      }
    },
    printReadableDate:function(date){
      return moment(date).format('LLLL');
    },
    exitClient:function(){
      localStorage.removeItem('clientId');
      this.client = {id:0,client_name:'',client_email:'',client_telephone:'',password:'',facebook_id:''}
      this.clientReservations = {};
      this.clientErrors = [];
    }
  },
  watch:{
    when:function(){
      console.log(this.when);

    },
    keyword:function(){
      axios.get('/api/suggest/'+this.keyword)
      .then(response => {
        this.tips = response.data
      })
    }
  },
  data: {
    client:{id:0,client_name:'',client_email:'',client_telephone:'',password:'',facebook_id:''},
    clientSaveStatus:'',
    clientReservations:{},
    currentClientAddress:{id:0,client_id:0,full_address:'',address_label:'',address_notes:''},
    clientAddresses:[],
    editingAddress:0,
    clientErrors:[],
    authStatus:'',
    searchMode:'',
    isSubmit:false,
    searchFormAction:'',
    keyword:'',
    where:'',
    when:'',
    lat:0,
    lng:0,
    address:(localStorage.getItem('fissaloAddress')!=null) ? localStorage.getItem('fissaloAddress') : '',
    radius:5,
    tips:[],
    hideAddressSelection:true,
    mainCities:main_cities,
    tagSelected:false,
  },
  mounted:function(){
    if(localStorage.getItem('clientId')>0){
      this.loadClient();
    }

    var placeAutocomplete = new google.maps.places.Autocomplete(

      {
        componentRestrictions: {country: "it"},
        rankBy: google.maps.places.RankBy.DISTANCE
      });
      placeAutocomplete.addListener('place_changed', function() {
        var place = placeAutocomplete.getPlace();
        app.address = place.name;
        localStorage.setItem('fissaloAddress', place.name);
        app.closeAddressSelection();
        if(app.isSubmit){
          app.goToResults();
        }
      })
    }
  })
  </script>
</body>
</html>
