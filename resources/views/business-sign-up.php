<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://erp.amtitalia.com/admin/js/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">

  <title>Hello, world!</title>

</head>
<body>



  <div id="app">
    <div class="container-fluid border-bottom">
      <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse row" id="navbarSupportedContent">
          <div class="col-3">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item " >
                <span class="pointer" @click="openAddressSelection">
                  <i class="fas fa-city"></i>
                  <span id="navbarAddressHolder">{{address.substring(0, 100)}}</span>
                </span>

                <a v-if="address!=''" href="javascript:" @click="unsetCity"><i class="far fa-times-circle"></i></a>
              </li>
            </ul>
          </div>
          <div class="col-6 text-center">
            <a class="navbar-brand"  href="/it/">
              <img src="/img/logo_fissalo_front.png" alt="">
            </a>
          </div>
          <div class="col-3 text-right">
            <a href="/it/business" class="btn btn-fissalo-secondary mr-1 btn-sm">
              BUSINESS
            </a>
            <a class="btn btn-fissalo-default mr-1 btn-sm">
              ACCEDI
            </a>
          </div>
        </div>
      </nav>
    </div>

    <form v-bind:action="searchFormAction" method="GET" autocomplete="off" @submit.prevent="checkAddress">
      <input type="hidden" v-model="lat">
      <input type="hidden" v-model="lng">
      <input type="hidden" v-model="radius">
      <div class="container-fluid text-center bg-light shadow-sm ">
        <div class="row no-gutters pt-2 pb-2">
          <div class="col-3">

          </div>
          <div class="col-md-3">
            <div class="input-group input-group-lg">
              <input  @keyup="resetSearch" required id="keywordInput" type="text" v-model="keyword" class="form-control border-top-0 border-left-0 border-bottom-0" placeholder="Cosa cerchi?" onfocus="this.placeholder=''" onblur="this.placeholder='Cosa cerchi?'">
            </div>

          </div>

          <div class="col-md-2">
            <div class="input-group input-group-lg">
              <input id="whenInput"type="text" v-model="when" class="form-control datepicker border-0" placeholder="Quando?" onfocus="this.placeholder=''" onblur="this.placeholder='Quando?'">
            </div>
          </div>
          <div class="col-md-1 text-left">
            <button type="submit" class="btn btn-fissalo-super-primary btn-lg btn-block"><i class="fa fa-search"></i></button>
          </div>
          <div class="col-1"></div>
        </div>
      </div>
      <div class="container" v-if="tips.length != 0 && tagSelected == false">
        <div class="row no-gutters">
          <div class="col-3">

          </div>
          <div class="col-6 bg-white shadow-sm">
            <div class="row">
              <div class="col-4">
                <div class="pl-2">
                  <h5 class="mt-2">Categorie</h5>
                  <ul class="list list-unstyled" v-if="tips.tags.length > 0">
                    <li v-for="tag in tips.tags">
                      <a class="autocompleteItem" href="javascript:" @click="autocompleteTag(tag.tag,'tag')">
                        {{tag.tag}}
                      </a>
                    </li>
                  </ul>
                </div>

              </div>
              <div class="col-4">
                <div class="pl-2">
                  <h5 class="mt-2">Professionisti</h5>
                  <ul class="list list-unstyled" v-if="tips.companies.length > 0">
                    <li v-for="company in tips.companies">
                      <a class="autocompleteItem" v-bind:href="companyUrl(company)">
                        {{company.company_name}}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-4">
                <div class="pl-2">
                  <h5 class="mt-2">Brands</h5>
                  <ul class="list list-unstyled" v-if="tips.brands.length > 0">
                    <li v-for="brand in tips.brands">
                      <a class="autocompleteItem" v-bind:href="brandUrl(brand)">
                        {{brand.brand_name}}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form @submit.prevent="confirmBusinessSignUp" v-if="registrationFeedback.status != 1">
      <div class="container mt-4 mb-4">
        <div class="row mb-4 mt-4">
          <div class="col-md-12 text-center">
            <h1>Unisciti ora a Fissalo.it</h1>
            <h3>E raggiungi immediatamente migliaia di nuovi clienti!</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Nome</label>
              <input type="text" class="form-control" v-model="formData.user.name" placeholder="" required>
            </div>


          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Cognome</label>
              <input type="text" class="form-control" v-model="formData.user.surname" placeholder="" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Nome attività</label>
              <input type="text" class="form-control" v-model="formData.company.company_name" id="" placeholder="" required>
            </div>

          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Categoria</label>
              <select class="form-control" name="" v-model="formData.company.businesstype_id" required>
                <option value="" selected>Scegli una categoria...</option>
                <option v-for="type in businessTypes" :value="type.id">{{type.business_type_name}}</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="">Indirizzo</label>
              <input id="CompanyAutocompleteAddress" type="text" class="form-control" v-model="formData.company.full_address" placeholder="" required>
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-md-3"></div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Email</label>
              <input type="text" class="form-control" v-model="formData.user.email" placeholder="" required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">Password</label>
              <input type="password" class="form-control" v-model="formData.user.password" placeholder="" required>
            </div>
          </div>
        </div>
        <div class="row" v-if="registrationFeedback.status==0">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <ul class="list m-0">
                <li v-for="error in registrationFeedback.errors">{{error}}</li>
              </ul>

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6 text-center">
            <button type="submit" class="btn btn-primary btn-block" name="button">Registrati</button>
          </div>
        </div>
      </div>
    </form>
    <div v-else>
      <div class="container mt-4 mb-4">
        <div class="row mb-4 mt-4">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="jumbotron">
              <h1 class="display-4">Congratulazioni!</h1>
              <p class="lead">La registrazione del tuo business è andata a buon fine!</p>
              <hr class="my-4">
              <p>Adesso fai click su continua per portare a termine l'attivazione!</p>
              <p class="lead">
                <a class="btn btn-primary btn-lg" href="/admin" role="button">Continua</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <h6>AZIENDA</h6>
            <ul class="list-unstyled">
              <li>Chi siamo</li>
              <li>Lavora con Noi</li>
              <li>Stampa</li>
              <li>Blog</li>
              <li>Contattaci</li>
            </ul>
          </div>
          <div class="col-md-2">
            <h6>HAI UN'ATTIVITA'?</h6>
            <ul class="list-unstyled">
              <li>Vantaggi</li>
              <li>Quanto Costa</li>
              <li>Proponi Attività</li>
            </ul>
          </div>
          <div class="col-md-3">

          </div>
          <div class="col-md-5">
            <h6>NEWSLETTER</h6>
            <div class="mb-4">
              <form id="newsletterSubscriptionForm" action="index.html" method="post">
                <div class="input-group">

                  <input type="email" class="form-control" placeholder="La tua email">
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-secondary" type="button">Invia</button>
                  </div>

                </div>
                <p class="help-block mt-2 text-small">Registrati inserendo il tuo indirizzo e-mail qui sotto per ricevere aggiornamenti.<br>Ci impegniamo a non inviare spam.</p>
              </form>
            </div>

            <ul class="list list-inline mt-4">
              <li class="list-inline-item">
                <a href="#"><i class="fab fa-2x fa-facebook-square mr-2"></i></a>
                <a href="#"><i class="fab fa-2x fa-instagram"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 text-white text-small">
            © 2019 Fissalo.it. All Right Are Reserved. P.Iva: IT012345566343
          </div>
          <div class="col-md-6 text-right">
            <ul class="list list-inline">
              <li class="list-inline-item text-small">
                <a href="#">Condizioni d'uso</a>
              </li>
              <li class="list-inline-item text-small">
                <a href="#">Privacy</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.5.22/dist/vue.js"></script>

  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('ADDRESS_AUTOCOMPLETE_API_KEY')?>&libraries=places,drawing&sensor=true&types=establishment"></script>
  <script type="text/javascript" src="https://erp.amtitalia.com/admin/js/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js" ></script>
  <link rel="stylesheet" href="https://www.mamaflorence.com/static/js/owl/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="https://www.mamaflorence.com/static/js/owl/dist/assets/owl.theme.default.min.css">
  <script type="text/javascript" src="https://www.mamaflorence.com/static/js/owl/dist/owl.carousel.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/it.js"></script>
  <script>

  $(function(){
    $('.datepicker').datepicker();
  })



  var app = new Vue({
    el: '#app',

    methods:{
      confirmBusinessSignUp:function(){
        axios
        .post('/api/business-sign-up', this.formData)
        .then(response => {
          this.registrationFeedback = response.data;
          console.log(response.data);
        })
      },
      slugify(string) {
        const a = 'àáäâãåăæçèéëêǵḧìíïîḿńǹñòóöôœṕŕßśșțùúüûǘẃẍÿź·/_,:;'
        const b = 'aaaaaaaaceeeeghiiiimnnnoooooprssstuuuuuwxyz------'
        const p = new RegExp(a.split('').join('|'), 'g')
        return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with ‘and’
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
      },
      companyUrl:function(company){
        var url = [];
        url.push(this.slugify(company.businesstype.business_type_name));
        url.push(this.slugify(company.company_name)+'-'+this.slugify(company.locality+' '+company.route));
        return '/it/'+url.join('/');
      },
      brandUrl:function(brand){
        var url = [];
        url.push(this.slugify(brand.businesstype.business_type_name));
        url.push(this.slugify(brand.brand_name));
        url.push('a');
        url.push(this.slugify(this.address));
        return '/it/'+url.join('-');
      },
      tagUrl:function(tag){
        var url = [];
        url.push(this.slugify(tag));
        url.push(this.slugify(this.address));
        return '/it/'+url.join('-');
      },
      goToResults(){
        switch(this.searchMode){
          case 'tag':
          this.searchFormAction = this.tagUrl(this.keyword );
          break;
        }
        location.href = this.searchFormAction
      },
      capitalizeFirstLetter(string){
        return string.charAt(0).toUpperCase() + string.slice(1);
      },
      formatDate(date){
        return moment(date).format('lll')
      },
      unsetCity(){
        localStorage.removeItem('fissaloAddress');
        this.address = '';
      },
      resetSearch:function(){
        this.tagSelected = false;
      },
      confirmReservationSubmit:function(){
        var postData = {client:this.client,cc:this.cc};
        axios
        .post('/api/confirm-reservation/',postData)
        .then(response => {
          console.log(response);
        })
      },
      confirmReservation:function(){
        if(this.selectedServices.length>0){
          $('#confirmReservationModal').modal('show');
        }else{
          alert('Seleziona dei servizi prima di prenotare')
        }

      },

      openAddressSelection:function(){
        this.hideAddressSelection = false;
      },
      closeAddressSelection:function(){
        this.hideAddressSelection = true;
      },
      autocompleteTag:function(keyword){
        this.keyword = keyword;
        this.tips = [];
      },
      search:function(){},
      submitReservation:function(){},

    },
    data: {
      registrationFeedback:{
        status:-1,
        user_id:'',
        errors:[]
      },
      searchFormAction:'',
      address:(localStorage.getItem('fissaloAddress')!=null) ? localStorage.getItem('fissaloAddress') : '',
      keyword:'',
      where:'Firenze',
      when:'',
      lat:0,
      lng:0,
      radius:5,
      tips:[],
      hideAddressSelection:true,
      formData:{company:{businesstype_id:''},user:{},'csrf-token':'<?=csrf_token()?>'},
      businessTypes:[]
    },

    watch:{
      selectedSchedule:function(newValue,oldValue){
        this.filterSlots(newValue)
      },
      keyword:function(){
        axios.get('/api/suggest/'+this.keyword)
        .then(response => {
          this.tips = response.data
        })
      }
    },

    mounted:function(){

      var placeAutocompleteCompanyAddress = new google.maps.places.Autocomplete(
        (document.getElementById('CompanyAutocompleteAddress')),
        {
          componentRestrictions: {country: "it"},
          rankBy: google.maps.places.RankBy.DISTANCE
        });



        axios.get('/api/load-business-types/')
        .then(response=>{

          app.businessTypes = response.data;
        })

        var placeAutocomplete = new google.maps.places.Autocomplete(
          (document.getElementById('placeAutocomplete')),
          {
            componentRestrictions: {country: "it"},
            rankBy: google.maps.places.RankBy.DISTANCE
          });
          placeAutocomplete.addListener('place_changed', function() {
            var place = placeAutocomplete.getPlace();
            console.log(place);
            app.address = place.name;
            app.closeAddressSelection();
          })
        }
      })
      </script>
    </body>
    </html>
