<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function companies(){
        return $this->belongsToMany('App\Company');
    }

    public function businesstype()
    {
        return $this->belongsTo('App\Businesstype');
    }
}
