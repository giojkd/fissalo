<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use Auth;

class Schedule extends Model
{
  //
  public function services()
  {
    return $this->belongsToMany('App\Service');
  }

  public function company()
  {
    return $this->belongsTo('App\Company');
  }



  public function reservationrow(){
    return $this->hasMany('App\Reservationrow');
  }

  public static function boot() {
    parent::boot();


    static::addGlobalScope('tenancy',function($builder){
      if(Auth::check()){
        $user = Auth::user();
        if($user->usertype_id != 1){
          $builder->where('company_id',$user->company_id);
        }

      }
    });



    static::created(function($schedule) {
      Event::fire('App\Events\ScheduleCreated', $schedule);
    });

    static::updated(function($schedule) {
      Event::fire('App\Events\ScheduleUpdated', $schedule);
    });

    static::deleted(function($schedule) {
      Event::fire('App\Events\ScheduleDeleted', $schedule);
    });
  }




}
