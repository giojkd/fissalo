<?php


namespace App\Handlers\Events;

use App\Schedule;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Schedulerow;
use DB;

class ScheduleEvents
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScheduleDeleted  $event
     * @return void
     */

     public function scheduleCreated(Schedule $event){
        $this->scheduleUpdated($event);
     }

     public function scheduleUpdated(Schedule $event){

        DB::table('schedule_service')->where('schedule_id',$event->id)->delete();
        if($event->services_column!=''){
          $services = explode(',',$event->services_column);
          foreach($services as $service_id){
            DB::table('schedule_service')->insert(['service_id'=>$service_id,'schedule_id'=>$event->id]);
          }
        }

         $schedule = json_decode($event->week_schedule,1);
         foreach($schedule as $day => $row){
            $day_of_the_week = substr($day,4,1);
            echo $day_of_the_week;
            $scheduleRow = \App\Schedulerow::firstOrCreate(['day_of_the_week' => $day_of_the_week,'schedule_id'=>$event->id]);

            if($row['status'] == 1){
                echo $row['closes_at'];
                $to = \Carbon\Carbon::createFromFormat('H:s', $row['closes_at']);
                $from = \Carbon\Carbon::createFromFormat('H:s', $row['opens_at']);
                $available_minutes = $to->diffInMinutes($from);
                if($row['ongoing'] == 0){
                    $to = \Carbon\Carbon::createFromFormat('H:s', $row['pause_ends_at']);
                    $from = \Carbon\Carbon::createFromFormat('H:s', $row['pause_starts_at']);
                    $pause_minutes = $to->diffInMinutes($from);
                    $available_minutes = $available_minutes-$pause_minutes;
                }


            }else{
                $available_minutes = 0;
            }

            $scheduleRow->available_minutes = $available_minutes;
            $scheduleRow->opens_at = $row['opens_at'];
            $scheduleRow->closes_at = $row['closes_at'];
            $scheduleRow->pause_starts_at = $row['pause_starts_at'];
            $scheduleRow->pause_ends_at = $row['pause_ends_at'];
            $scheduleRow->status = $row['status'];
            $scheduleRow->ongoing = $row['ongoing'];
            $scheduleRow->save();


         }

    }
}
