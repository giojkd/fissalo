<?php

namespace App\Handlers\Events;

use App\Reservationrow;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use DB;

class ReservationrowEvents
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function ReservationrowCreated(Reservationrow $reservationrow){
         $rr = $reservationrow;
         $service = \App\Service::find($reservationrow->service_id);
         $rr->reservation_end = date('Y-m-d H:i:s',strtotime($reservationrow->reservation_begin.' +'.$service->service_duration.'minutes'));
         $rr->save();

         $rrDay = date('Y-m-d',strtotime($rr->reservation_begin));

         $scheduleAvailability = \App\Scheduleavailability::firstOrCreate(
           ['schedule_id'=>$rr->schedule_id],['day'=>$rrDay]
         );

         $aux = DB::table('reservationrows')->selectRaw('SUM(TIMESTAMPDIFF(MINUTE,reservation_begin,  reservation_end)) as busy_minutes')->whereRaw("schedule_id = ".$rr->schedule_id." AND DATE(reservation_begin) = '".$rrDay."'")->first();

         $scheduleAvailability->busy_minutes = $aux->busy_minutes;
         $scheduleAvailability->save();

    }

    public function ReservationrowDeleted(Reservationrow $reservationrow){

      $rr = $reservationrow;
      $rrDay = date('Y-m-d',strtotime($rr->reservation_begin));

      $aux = DB::table('reservationrows')->selectRaw('SUM(TIMESTAMPDIFF(MINUTE,reservation_begin,  reservation_end)) as busy_minutes')->whereRaw("schedule_id = ".$rr->schedule_id." AND DATE(reservation_begin) = '".$rrDay."'")->first();

      $scheduleAvailability = \App\Scheduleavailability::firstOrCreate(
        ['schedule_id'=>$rr->schedule_id],['day'=>$rrDay]
      );

      $scheduleAvailability->busy_minutes = $aux->busy_minutes;
      $scheduleAvailability->save();
    }


    public function ReservationrowUpdated(Reservationrow $reservationrow){
      /*
      $rr = \App\Reservationrow::find($reservationrow->id);
      $service = \App\Service::find($reservationrow->schedule_id);
      $rr->reservation_end = date('Y-m-d H:i:s',strtotime($reservationrow->reservation_begin.' +'.$service->service_duration.'minutes'));
      $rr->save();
      */
    }

}
