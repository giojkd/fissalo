<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicecategory extends Model
{
  public function businesstype()
  {
      return $this->belongsTo('App\Businesstype');
  }
}
