<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reservation extends Model
{



  public function client()
  {
    return $this->belongsTo('App\Client');
  }

  public function address(){
    return $this->belongsTo('App\Address');
  }
  public function company(){
    return $this->belongsTo('App\Company');
  }

  public function allCompanies(){
    return $this->belongsTo('App\Company')->withoutGlobalScopes();
  }



  public function reservationrows(){
    return $this->hasMany('App\Reservationrow');
  }

  public static function boot(){

    parent::boot();
    static::addGlobalScope('tenancy',function($builder){
      if(Auth::check()){
        $user = Auth::user();
        if($user->usertype_id != 1){
          $builder->where('company_id',$user->company_id);
        }
      }
    });
  }
}
