<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Businesstype extends Model
{
    //
    public function businesscategory(){
    	return $this->belongsTo('App\Businesscategory');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
}
