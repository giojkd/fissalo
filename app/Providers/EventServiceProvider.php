<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\CompanyCreated' => [
            'App\Handlers\Events\CompanyEvents@companyCreated',
        ],
        'App\Events\CompanyUpdated' => [
            'App\Handlers\Events\CompanyEvents@companyUpdated',
        ],
        'App\Events\CompanyDeleted' => [
            'App\Handlers\Events\CompanyEvents@companyDeleted',
        ],
        'App\Events\ScheduleCreated' => [
            'App\Handlers\Events\ScheduleEvents@scheduleCreated',
        ],
        'App\Events\ScheduleUpdated' => [
            'App\Handlers\Events\ScheduleEvents@scheduleUpdated',
        ],
        'App\Events\ScheduleDeleted' => [
            'App\Handlers\Events\ScheduleEvents@scheduleDeleted',
        ],
        'App\Events\ReservationrowUpdate' => [
            'App\Handlers\Events\ReservationrowEvents@ReservationrowUpdated',
        ],
        'App\Events\ReservationrowCreated' => [
            'App\Handlers\Events\ReservationrowEvents@ReservationrowCreated',
        ],
        'App\Events\ReservationrowDeleted' => [
            'App\Handlers\Events\ReservationrowEvents@ReservationrowDeleted',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
