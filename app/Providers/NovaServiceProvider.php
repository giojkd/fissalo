<?php

namespace App\Providers;

use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use Mgc\Scheduler\Scheduler;
use Mgc\FirstCard\FirstCard;

use Auth;

use App\Nova\Address;
use App\Nova\Brand;
use App\Nova\Businesscategory;
use App\Nova\Businesstype;
use App\Nova\Client;
use App\Nova\Company;
use App\Nova\Reservation;
use App\Nova\Reservationrow;
use App\Nova\Resource;
use App\Nova\Schedule;
use App\Nova\Service;
use App\Nova\Servicecategory;
use App\Nova\Tag;
use App\Nova\User;
use App\Nova\Usertype;




class NovaServiceProvider extends NovaApplicationServiceProvider
{
  /**
  * Bootstrap any application services.
  *
  * @return void
  */
  public function boot()
  {
    parent::boot();

    #Nova::style('fissalo',asset('css/fissalo.css')); #DO NOT PUBLISH
  }

  /**
  * Register the Nova routes.
  *
  * @return void
  */
  protected function routes()
  {
    Nova::routes()
    ->withAuthenticationRoutes()
    ->withPasswordResetRoutes()
    ->register();
  }

  /**
  * Register the Nova gate.
  *
  * This gate determines who can access Nova in non-local environments.
  *
  * @return void
  */
  protected function gate()
  {
    Gate::define('viewNova', function ($user) {
      return in_array($user->email, [
        //
      ]);
    });
  }

  /**
  * Get the cards that should be displayed on the Nova dashboard.
  *
  * @return array
  */
  protected function cards()
  {
    return [
      new FirstCard
    ];
  }

  /**
  * Get the tools that should be listed in the Nova sidebar.
  *
  * @return array
  */
  public function tools()
  {
    return [
      #new \Cloudstudio\ResourceGenerator\ResourceGenerator(),
      new Scheduler
    ];
  }

  /**
  * Register any application services.
  *
  * @return void
  */
  public function register()
  {
    //
  }



  public function resources(){
    $user = Auth::user();
    switch($user->usertype_id){
      case 1:
        $resource = [
          User::class,
          Address::class,
          Brand::class,
          Businesscategory::class,
          Businesstype::class,
          Client::class,
          Company::class,
          Reservation::class,
          Reservationrow::class,
          Schedule::class,
          Service::class,
          Servicecategory::class,
          Tag::class,
          User::class,
          Usertype::class,
        ];
      break;
      case 2:
        $resource = [
          Businesscategory::class,
          Businesstype::class,
          Company::class,

        ];

        $company = $user->company;
        if($user->step_1_ok()){
          $resource[] = Service::class;
          $resource[] = Servicecategory::class;
        }
        if($user->step_2_ok()){
          $resource[] = Schedule::class;
        }
        $schedules = \App\Schedule::where('company_id',$company->id)->get();
        if($user->step_3_ok()){
          $resource[] = Address::class;
          $resource[] = Client::class;
          $resource[] = Reservation::class;
          $resource[] = Reservationrow::class;
        }
      break;
      case 3:

      break;
    }
    Nova::resources($resource);
  }
}
