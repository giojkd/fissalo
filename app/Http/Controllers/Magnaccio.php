<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Reservationrow;
use Cartalyst\Stripe\Stripe;




class Magnaccio extends Controller
{

  public function editClientAddress(Request $request){
    $data = $request->all();
    $data['address_label'] = ($data['address_label'] != '') ? $data['address_label'] : 'Indirizzo principale';
    if($data['id'] == 0){
      $address = new \App\Address();
      $address->fill($data);
      $address->save();
    }else{
      $address = \App\Address::findOrFail($data['id']);
      unset($data['id']);
      $address->fill($data);
      $address->save();
    }
    return ['status'=>1];
  }

  public function deleteClientAddress(Request $request){
    $data = $request->all();
    $address = \App\Address::findOrFail($data['id']);
    $address->delete();
  }

  public function deleteReservation(Request $request){
    $data = $request->all();
    \App\Reservationrow::find($data['id'])->delete();
    return $data;
  }

  public function loadClient(Request $request){
    $client = \App\Client::with(['addresses','reservations','reservations.company','reservations.reservationrows','reservations.reservationrows.schedule','reservations.reservationrows.service'])->find($request->input('id'));

    $returnReservations = [];
    if(!is_null($client->reservations)){
      foreach($client->reservations as $reservation){

        if(!is_null($reservation->reservationrows)){
          foreach ($reservation->reservationrows as $reservationrow) {
            $buttons = [];
            if(!Carbon::parse($reservationrow->reservation_begin)->isPast()){
              $buttons[] = '<a onclick="app.deleteReservation('.$reservationrow->id.')" class="btn btn-outline-danger btn-sm" href="javascript:"><i class="fa fa-trash"></i> Cancella</a>';
            }
            $item = [
              'company' => $reservation->company->company_name,
              'service' => $reservationrow->service->service_name,
              'time' => $reservationrow->reservation_begin,
              'price' => ($reservationrow->service_price >0 ) ? $reservationrow->service_price : 0,
              'payment_label' => ($reservation->payment_online) ? 'Pagato online' : 'Pagamento in negozio',
              'payment_label_class' => ($reservation->payment_online) ? 'badge-success' : 'badge-warning',
              'schedule' => $reservationrow->schedule->schedule_name,
              'buttons' => $buttons
            ];
            $returnReservations[] = $item;
          }
        }
      }
    }


    $addresses = [];
    if(!is_null($client->addresses)){
      foreach($client->addresses as $address){
        $addresses[] = [
          'id' => $address->id,
          'full_address' => $address->full_address,
          'address_notes' => $address->address_notes,
          'address_label' => $address->address_label,
          'client_id' => $address->client_id
        ];
      }
    }

    #return $client->client_date_of_birth;

    $client_date_of_birth = (!is_null($client->client_date_of_birth)) ? $client->client_date_of_birth->format('d/m/Y') : NULL;

    $return = [
      'id' => $client->id,
      'client_name' => $client->client_name.' '.$client->client_surname,
      'client_email' => $client->client_email,
      'client_telephone' => $client->client_telephone,
      'facebook_id' => $client->facebook_id,
      'client_date_of_birth' => $client_date_of_birth,
    ];

    return ['status' => 1,'client'=>$return,'reservations'=>$returnReservations,'addresses'=>$addresses];
  }

  public function updateClient(Request $request){

    $data = $request->all();
    if($data['client_name']==''){
      return ['status'=>0,'message'=>'Il nome è obbligatorio'];
    }
    if($data['client_email']==''){
      return ['status'=>0,'message'=>'La mail è obbligatoria'];
    }
    if(isset($data['client_date_of_birth'])){
      if($data['client_date_of_birth'] != ''){
        try{
          $date = Carbon::createFromFormat('d/m/Y', $data['client_date_of_birth']);
          $data['client_date_of_birth'] = $date->format('Y-m-d');
        }catch(\Exception $e){
          return ['status'=>0,'message'=>'La data inserita non è valida, usa il formato gg/mm/aaaa'];
        }
      }
    }

    if(isset($data['password'])){
      if(trim($data['password']) != ""){
        $data['password'] = Hash::make($data['password']);
      }
    }

    $client_name = array_slice(explode(' ',trim($data['client_name'])),0,2);
    if(count($client_name)<2){
      $client_name[] = '';
    }
    list($data['client_name'],$data['client_surname']) =  $client_name;
    $client = \App\Client::findOrFail($data['id']);
    unset($data['id']);
    $client->fill($data)->save();

    return ['status'=>1];
  }

  public function signUpClient(Request $request){
    $data = $request->all();

    if($data['password'] != ''){

      $client = \App\Client::where('client_email',$data['client_email'])->first();
      if(!is_null($client)){
        return ['status'=>0,'client'=>['id'=>0,'client_name'=>$data['client_name'],'client_email'=>$data['client_email'],'client_telephone'=>'','facebook_id'=>'']];
      }else{
        $client_name = array_slice(explode(' ',trim($data['client_name'])),0,2);
        if(count($client_name)<2){
          $client_name[] = '';
        }
        list($data['client_name'],$data['client_surname']) =  $client_name;
        $data['password'] = Hash::make($data['password']);
        $client = new \App\Client();
        $client->fill($data);
        $client->save();

        $return = [
          'id' => $client->id,
          'client_name' => $client->client_name.' '.$client->client_surname,
          'client_email' => $client->client_email,
          'client_telephone' => $client->client_telephone,
          'facebook_id' => $client->facebook_id
        ];

        return ['status' => 1,'client'=>$return];

      }

    }else{
      return ['status'=>0,'client'=>['id'=>0,'client_name'=>$data['client_name'],'client_email'=>$data['client_email'],'client_telephone'=>'','facebook_id'=>'']];
    }
  }

  public function authClient(Request $request){

    $data = $request->all();


    if($data['facebook_id'] != ''){

      $client = \App\Client::where('facebook_id',$data['facebook_id'])->first();

      if(is_null($client)){
        list($data['client_name'],$data['client_surname']) =  array_slice(explode(' ',trim($data['client_name'])),0,2);
        $client = new \App\Client();
        $client->fill($data);
        $client->save();
      }

      $return = [
        'id' => $client->id,
        'client_name' => $client->client_name.' '.$client->client_surname,
        'client_email' => $client->client_email,
        'client_telephone' => $client->client_telephone,
        'facebook_id' => $client->facebook_id
      ];

      return ['status' => 1,'client'=>$return];

    }else{
      $status = 0;
      $client = \App\Client::where('client_email',$data['client_email'])->first();


      if(!is_null($client)){
        if(Hash::check($data['password'],$client->password)){
          $return = [
            'id' => $client->id,
            'client_name' => $client->client_name.' '.$client->client_surname,
            'client_email' => $client->client_email,
            'client_telephone' => $client->client_telephone,
            'facebook_id' => $client->facebook_id
          ];

          if(!is_null($client->client_date_of_birth)){
            $return['client_date_of_birth'] = $client->client_date_of_birth->format('d-m-Y');
          }

          return ['status' => 1,'client'=>$return];
        }else{
          return ['status'=>0,'client'=>['id'=>0,'client_name'=>'','client_email'=>$data['client_email'],'client_telephone'=>'','facebook_id'=>'']];
        }
      }else{
        return ['status'=>0,'client'=>['id'=>0,'client_name'=>'','client_email'=>$data['client_email'],'client_telephone'=>'','facebook_id'=>'']];
      }
    }
  }

  public function businessSignUp(Request $request){

    $errors = [];
    $user_id = 0;

    $input = $request->all();
    $company_ = $input['company'];
    $user_ = $input['user'];

    if(strlen($user_['password'])<8){
      $errors[] = 'La password deve essere lunga almeno 8 caratteri';
    }

    $sherlock = new Sherlock();
    $address = $sherlock->geocodeAddress($company_['full_address'])['results'][0];
    dd($address);
    $addressComponents = $sherlock->explodeAddress($address);


    if(!is_null(\App\Company::withoutGlobalScopes()->where('company_name',$company_['company_name'])->where('locality',$addressComponents['locality']['l'])->where('route',$addressComponents['route']['l'])->first())){
      $errors[] = "Esiste già un business come il tuo";
    }else{
      $company_['locality'] = $addressComponents['locality']['l'];
      $company_['route'] = $addressComponents['route']['l'];
      $company_['street_number'] = $addressComponents['street_number']['l'];
      $company_['administrative_area_level_1'] = $addressComponents['administrative_area_level_1']['l'];
      $company_['administrative_area_level_2'] = $addressComponents['administrative_area_level_2']['l'];
      $company_['administrative_area_level_3'] = $addressComponents['administrative_area_level_3']['l'];
      $company_['postal_code'] = $addressComponents['postal_code']['l'];
      $company_['full_address'] = $addressComponents['full_address'];
      $company_['lat'] = $addressComponents['lat'];
      $company_['lng'] = $addressComponents['lng'];
    }


    if(!is_null(\App\User::where('email',$user_['email'])->first())){
      $errors[] = "Esiste già un utente con questa email";
    }else {
      $user_['password'] =  \Hash::make($user_['password']);
    }

    $status = (count($errors)==0) ? 1 : 0;

    if($status == 1){
      $company_id = \DB::table('companies')->insertGetId($company_);
      $user_['company_id'] = $company_id;
      $user_['usertype_id'] = 2;
      $user = \App\User::create($user_);
      $user_id = $user->id;
    }



    $return = ['status'=>$status,'user_id'=>$user_id,'errors'=>$errors];
    return $return;
  }

  public function signUpBusiness(){
    return view('business-sign-up');
  }

  public function thankYou(){
    return view('thank-you');
  }

  public function confirmReservation(Request $request){
    $client = $request->input('client');
    $card = $request->input('cc');
    $notes = $request->input('notes');
    $payment_method_id = $request->input('payment_method');
    $reservation_id = $request->input('reservation_id');
    $reservation = \App\Reservation::withoutGlobalScopes()->with(['reservationrows'])->findOrFail($reservation_id);
    $reservation->company_id =  $request->input('company_id');
    $reservation_total = 0;
    $payment_online = 0;
    $payment_online_status = 0;
    foreach($reservation->reservationrows as $row){
      $reservation_total+=$row->service_price;
    }

    $errors = [];

    switch($payment_method_id){
      case 1:
      $payment_online = 1;

      $testKey = "sk_test_Clj4w7X7OghV3IKM5uboNS1X";
      $productionKey = "sk_live_DxsEdAUV4AaxRLokuPTGECiK";

      $key = $testKey;

      $stripe = Stripe::make($key);
      try {
        $token = $stripe->tokens()->create([
          'card' => [
            'number' => $card['num'],
            'exp_month' => $card['mm'],
            'exp_year' => $card['yy'],
            'cvc' => $card['ccv'],
          ],
        ]);

        $charge = $stripe->charges()->create([
          'card' => $token['id'],
          'currency' => 'EUR',
          'amount' => $reservation_total,
          'description' => 'Reservation '.$reservation_id,
        ]);

        if($charge['status'] == 'succeeded') {
          $status = 1;
          $payment_online_status = 1;
        }else{
          $status = 0;
        }
      }
      catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
        $errors[] =  $e->getMessage();
        $status = 0;
      }
      catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
        $errors[] =  $e->getMessage();
        $status  = 0;
      }
      break;
      case 2:
      $status = 1;
      break;
    }

    if($status == 1){
      $reservation->reservation_total = $reservation_total;
      $reservation->payment_online = $payment_online;
      $reservation->payment_online_status = $payment_online_status;
      $reservation->delivery_service = 0;
      $reservation->reservation_notes = $notes;

      #check client existence

      $clientCheck = \App\Client::where('client_email',$client['client_email'])->first();

      if(!is_null($clientCheck)){
        $client_id = $clientCheck->id;
      }else{
        $clientModel = \App\Client::create($client);
        $client_id = $clientModel->id;
      }

      $reservation->client_id = $client_id;
      $reservation->save();

      //$new_reservation = \App\Reservation::create([]);
      //$new_reservation_id = $new_reservation->id;
    }else{
      $new_reservation_id = 0;
    }

    $return = [
      'request' => $request,
      'status' => $status,
      'errors' => implode(', ',$errors),
      'reservation_id' => $reservation->id
    ];

    return $return;

  }

  public function loadReservationRows($id){
    return
    \App\Reservationrow::withoutGlobalScopes()
    ->with([
      'service'=>function($query){
        $query->withoutGlobalScopes();
      },
      'schedule'=>function($query){
        $query->withoutGlobalScopes();
      }])
      ->where('reservation_id',$id)
      ->get();
    }

    public function getReservationId(){
      $reservation  = new \App\Reservation;
      $reservation->save();
      $return = ['id' => $reservation->id];
      return json_encode($return);
    }

    public function setReservationRow(Request $request){
      $row = $request->all();

      $service = \App\Service::withoutGlobalScopes()->find($row['service_id']);

      $beginDay = Carbon::parse($row['day'])->format('Y-m-d');
      $beginTime = Carbon::parse($row['time'])->format('H:i:s');
      $reservationBegin = Carbon::parse($beginDay.' '.$beginTime);
      #$reservation_end = $reservationBegin->addMinutes($service->service_duration);

      $aux = date('Y-m-d H:i:s',strtotime($reservationBegin->format('Y-m-d H:i:s').' +'.$service->service_duration.'minutes'));

      $reservationRow = new \App\Reservationrow();
      $reservationRow->reservation_begin = $reservationBegin->format('Y-m-d H:i:s');
      #$reservationRow->reservation_end = $aux;
      $reservationRow->reservation_id = $row['reservation_id'];
      $reservationRow->service_id = $row['service_id'];
      $reservationRow->schedule_id = $row['schedule_id'];
      $reservationRow->service_price = $service->service_price;
      $reservationRow->save();

      $rr = \App\Reservationrow::withoutGlobalScopes()
      ->with([
        'service'=>function($query){
          $query->withoutGlobalScopes();
        },
        'schedule'=>function($query){
          $query->withoutGlobalScopes();
        }])
        ->find($reservationRow->id);

        $return = [
          'reservationRow' => $rr,
        ];

        return $return;
      }

      public function unsetReservationRow(Request $request){
        $row = $request->all();
        $reservationRow = \App\Reservationrow::where('reservation_id',$row['reservation_id'])->where('service_id',$row['service_id'])->first();
        $reservationRow ->delete();
      }
    }
