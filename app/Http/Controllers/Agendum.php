<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Auth;
use DB;
use App\Reservationrow;
class Agendum extends Controller
{

	public function getDaysIntervalAvailabilityForService($service_id, $day_begin, $day_end){
		$dayAvailable = [];
		$dayBeginFormatted = date('Ymd',strtotime($day_begin));
		$dayEndFormatted = date('Ymd',strtotime($day_end));
		$service = \App\Service::withoutGlobalScopes()->with(['schedules'])->findOrFail($service_id);
		$countDays = 0;
		while($dayBeginFormatted <= $dayEndFormatted){
			$dayOfTheWeek = date('N',strtotime($dayBeginFormatted));
			$dayFormatted = date('Y-m-d',strtotime($dayBeginFormatted));
			$schedules = DB::table('schedules')
			->select(
				DB::raw("schedules.id"),
				DB::raw("COALESCE((SELECT busy_minutes+".$service->service_duration." FROM scheduleavailabilities WHERE scheduleavailabilities.schedule_id = schedules.id AND scheduleavailabilities.day = '".$dayFormatted."'),0) as busy_minutes"),
				'schedulerows.available_minutes'
				)
				->leftJoin('schedule_service','schedules.id','=','schedule_service.schedule_id')
				->leftJoin('schedulerows','schedules.id','=','schedulerows.schedule_id')
				->whereRaw('schedule_service.service_id = '.$service_id)
				->whereRaw('schedulerows.day_of_the_week = '.$dayOfTheWeek)
				->whereRaw('schedulerows.status = 1')
				->havingRaw('schedulerows.available_minutes > busy_minutes')
				->get();
				$dayAvailable[$dayBeginFormatted]  = $schedules;
				$countDays++;
				$dayBeginFormatted = date('Ymd',strtotime($dayBeginFormatted.' +1days'));
			}

			$ra = [];
			if(count($dayAvailable)>0){

				foreach($dayAvailable as $index =>  $da){
					if(count($da)>0){
						$ra[$index] = true;
					}else{
						$ra[$index] = false;
					}
				}
			}

			return $ra;
		}

		public function getDaySlotsForService($day, $service_id){
			$return = [
				'day' => $day,
				'service_id' => $service_id
			];
			$slotResults = [];
			$schedules = [];

			$dayOfTheWeek = date('N',strtotime($day));
			$dayFormatted = date('Y-m-d',strtotime($day));

			$service = \App\Service::withoutGlobalScopes()->with(['schedules'])->findOrFail($service_id);

			$schedules = DB::table('schedules')
			->select(
				'schedules.id',
				'schedules.schedule_name',
				'schedulerows.opens_at',
				'schedulerows.closes_at',
				'schedulerows.pause_starts_at',
				'schedulerows.pause_ends_at',
				'schedulerows.ongoing',
				DB::raw("COALESCE((SELECT busy_minutes+".$service->service_duration." FROM scheduleavailabilities WHERE scheduleavailabilities.schedule_id = schedules.id AND scheduleavailabilities.day = '".$dayFormatted."'),0) as busy_minutes"),
				'schedulerows.available_minutes'
				)
				->leftJoin('schedule_service','schedules.id','=','schedule_service.schedule_id')
				->leftJoin('schedulerows','schedules.id','=','schedulerows.schedule_id')
				->whereRaw('schedule_service.service_id = '.$service_id)
				->whereRaw('schedulerows.day_of_the_week = '.$dayOfTheWeek)
				->whereRaw('schedulerows.status = 1')
				->havingRaw('schedulerows.available_minutes > busy_minutes')
				->get();

				#echo json_encode($schedules);
				$allAvailableSlots = [];
				$slotsBySchedule = [];
				$company = \App\Company::withoutGlobalScopes()->findOrFail($service->company_id);

				if(!$schedules->isEmpty()){
					foreach($schedules as $schedule){
						$scheduleDayReservations = \App\Reservationrow::where('schedule_id',$schedule->id)->whereRaw("DATE(reservation_begin) = '".$dayFormatted."'")->get();

						$scheduleSlots = [];
						$lastSlot = $schedule->closes_at;
						$currentSlot = $schedule->opens_at;
						while(strtotime($currentSlot)<strtotime($lastSlot)){
							$isIncluded = 0;

							$tmp = [];

							if(!$scheduleDayReservations->isEmpty()){
								foreach($scheduleDayReservations as $reservation){
									$reservationBegin = $reservation->reservation_begin;
									$reservationEnd = $reservation->reservation_end;
									$tmp[] = [
										'slot'=> $currentSlot,
										'slotTime' => strtotime($currentSlot),
										'reservationBegin' =>$reservationBegin->format('H:i'),
										'reservationEnd' =>$reservationEnd->format('H:i'),
									];
									if(strtotime($currentSlot) > strtotime($reservationBegin->format('H:i')) && strtotime($currentSlot) < strtotime($reservationEnd->format('H:i'))){
										$isIncluded++;
									}
								}
							}
							if($isIncluded==0){
								$scheduleSlots[] = $currentSlot;
							}
							$currentSlot = date('H:i:s',strtotime($currentSlot.'+'.$company->slot_step.'minutes'));
						}

						$allAvailableSlots = array_merge($allAvailableSlots,$scheduleSlots);
					}
					$slotsBySchedule[$schedule->id] = $scheduleSlots;
					$allAvailableSlots = array_unique($allAvailableSlots);
					#unset($availableSlots[count($availableSlots)-1]);

					$schedulesBySlot = [];
					foreach($slotsBySchedule as $schedule_id => $slots){
						foreach($slots as $slot){
							$schedulesBySlot[$slot][] = $schedule_id;
						}
					}

					foreach($schedulesBySlot as $time => $timeSchedules){
						$slotResults[] = [
							'show' => true,
							'schedules' => $timeSchedules,  #[1,3] where 1 and 3 ar schedules id
							'time' => substr($time,0,5) #this time slot is shared by both schedule 1 and 3
						];
					}
				}

				$return['availableSlots'] = $slotResults;
				$return['availableSchedules'] = $schedules;
				echo json_encode($return);
			}

			public function getReservationsRows(Request $request){
				$start = $request->input('start');
				$end = $request->input('end');
				$return = [];
				$reservations = \App\Reservationrow::with(['service','schedule'])->whereBetween('reservation_begin',[$start,$end])->get();
				foreach($reservations as $row){
					//$end = Carbon::parse(substr($row->reservation_day,0,10).' '.$row->service_time)->addMinutes(60);

					$return[] = [
						'id' => $row->id,
						'title' => $row->service->service_name,
						'start' => str_replace(' ','T',$row->reservation_begin),
						'end' => str_replace(' ','T',$row->reservation_end),
						'eventType' => 'reservation_row'
					];
				}
				return $return;
			}


			public function getReservationDataModal(Request $request){

				$reservation = \App\Reservationrow::with(['reservation','reservation.client'])->where('id',$request->input('id'))->first();

				$services = \App\Service::where('company_id',$reservation->reservation->company_id)->get();
				$schedules = \App\Schedule::where('company_id',$reservation->reservation->company_id)->get();


				return [
					'reservation' => $reservation,
					'services' => $services,
					'schedules' => $schedules
				];


			}

			public function loadService(Reqeust $request){
				$id = $request->input('id');
				return \App\Service::findOrFail($id);
			}

			public function loadAddress(Request $request){
				$id = $request->input('id');
				return \App\Address::findOrFail($id);
			}
			public function loadClientData(Request $request){
				$id = $request->input('id');
				return \App\Client::with('addresses')->findOrFail($id);
			}

			public function loadEventModal(Request $request){
				$data = [];

				$id = $request->input('id');

				$userId = $request->input('user_id');
				$user = \App\User::findOrFail($userId);

				$reservationrow = \App\Reservationrow::with(['reservation','reservation.client','reservation.client.addresses'])->findOrFail($id);
				$reservation = $reservationrow->reservation;

				$services = \App\Service::where('company_id',$user->company_id)->get();
				$schedules = \App\Schedule::where('company_id',$user->company_id)->get();
				$client = $reservation->client;


				$data['client'] = $client;
				$data['available_services'] = $services;
				$data['available_schedules'] = $schedules;
				$data['reservation'] = $reservation;
				$data['reservationrow'] = $reservationrow;



				return view('event-modal', $data);
			}

			public function loadModal(Request $request){
				$data = [];

				$type = $request->input('type');
				$id = $request->input('id');

				$userId = $request->input('user_id');
				$user = \App\User::findOrFail($userId);


				$company = \App\Company::withoutGlobalScopes()->findOrFail($user->company_id);
				$services = \App\Service::where('company_id',$user->company_id)->get();
				$schedules = \App\Schedule::where('company_id',$user->company_id)->get();
				$clients = $company->clients;

				if($id>0){
					$reservationrow = \App\Reservationrow::with(['reservation','reservation.client','reservation.client.addresses'])->findOrFail($id);

					$reservation = $reservationrow->reservation;
					$reservationrows = $reservation->reservationrows;

					$data['client'] = $reservation->client;
					$data['available_services'] = $services;
					$data['available_schedules'] = $schedules;
					$data['available_clients'] = 	$clients;
					$data['rows'] = $reservationrows;
					$data['company'] = $company;
					$data['reservation'] = $reservation;

				}else{
					$data['client'] = new \App\Client();
					$data['available_services'] = $services;
					$data['available_schedules'] = $schedules;
					$data['available_clients'] = 	$clients;
					$data['rows'] = [];
					$data['company'] = $company;
					$data['reservation'] = new \App\Reservation();
				}

				return view('calendar-modal', $data);
			}

			public function moveReservation(Request $request){

				$id = $request->input('id');
				$begin = trim($request->input('start'),'"');
				$end = trim($request->input('end'),'"');
				$method = $request->input('method');

				$reservation = \App\Reservationrow::with(['service'])->findOrFail($id);


				$reservation->reservation_begin = \str_replace('T',' ',$begin);
				$reservation->reservation_end = \str_replace('T',' ',$end);


				echo $reservation->reservation_begin;
				echo $reservation->reservation_end;

				$reservation->save();




			}

			public function updateReservationRow(Request $request){

			}
		}
