<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Geocoder\Laravel\ProviderAndDumperAggregator as Geocoder;
class Sherlock extends Controller
{
  //

  public function explodeAddress($addr){
    foreach($addr['address_components'] as $comp){
      foreach($comp['types'] as $type){
        $return[$type] = ['l'=>$comp['long_name'],'s'=>$comp['short_name']];
      }
    }
    $return['full_address'] = $addr['formatted_address'];
    $return['lat'] = $addr['geometry']['location']['lat'];
    $return['lng'] = $addr['geometry']['location']['lng'];
    return $return;
  }

  public function geocodeAddress($addr){
    $cleanAddress = str_replace (" ", "+", $addr);
    $details_url = "https://maps.googleapis.com/maps/api/geocode/json?key=".env('GOOGLE_MAPS_GEOCODE')."&address=".$cleanAddress."&sensor=false";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $details_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $geoloc = json_decode(curl_exec($ch), true);
    return $geoloc;
  }

  public function getGeoLocation($addr)
  {
    $geoloc = $this->geocodeAddress($addr);
    switch ($geoloc['status']) {
      case 'ZERO_RESULTS':
      return 0;
      break;
      case 'OK':
      return $geoloc['results'][0]['geometry']['location'];
      break;
    }
  }

  public function getBusinessSlots($date,$businesses,$countSlots = 4){
    $dayOfTheWeek = date('N',strtotime($date));
    $companies =
    \App\Company::withoutGlobalScopes()
    ->with(
      [
        'schedule',
        'schedule.reservationrow'=>function($query) use ($date){
          $query->whereDate('reservationrows.reservation_begin',$date);
        }
      ]
      )
      ->whereIn('id',explode(',',$businesses))->get();
      $results = [];
      if(!$companies->isEmpty()){
        foreach($companies as $company){
          if(!$company->schedule->isEmpty()){
            foreach($company->schedule as $schedule){
              $scheduleSlots = [];
              $weekSchedule = json_decode($schedule->week_schedule,1);
              #echo '<pre>';
              #print_r($weekSchedule);
              $slots = [];
              $daySchedule = $weekSchedule['day_'.$dayOfTheWeek];
              $lastSlot = $daySchedule['closes_at'];
              $currentSlot = date('H:i:s',strtotime($daySchedule['opens_at']));

              while(strtotime($currentSlot)<strtotime($lastSlot)){
                $isIncluded = 0;

                $tmp = [];

                if(!$schedule->reservationrow->isEmpty()){
                  foreach($schedule->reservationrow as $reservation){
                    $reservationBegin = $reservation->reservation_begin;
                    $reservationEnd = $reservation->reservation_end;
                    if(strtotime($currentSlot) > strtotime($reservationBegin->format('H:i')) && strtotime($currentSlot) < strtotime($reservationEnd->format('H:i'))){
                      $isIncluded++;
                    }
                  }
                }
                if($isIncluded==0){
                  $scheduleSlots[] = $currentSlot;
                }
                $currentSlot = date('H:i:s',strtotime($currentSlot.'+'.$company->slot_step.'minutes'));
              }
            }
          }
          $scheduleSlots = array_unique($scheduleSlots);
          $scheduleSlotsReturn = [];
          if(count($scheduleSlots)){
            foreach($scheduleSlots as $ss){
              $scheduleSlotsReturn[] = ['value' => substr($ss, 0 , 5)] ;
            }
          }
          $results[] = [
            'company_id' => $company->id,
            'slots' => array_slice($scheduleSlotsReturn,0,$countSlots)
          ];
        }
      }
      return $results;



    }

    public static function slugify($text)
    {
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
      $text = preg_replace('~[^-\w]+~', '', $text);
      $text = trim($text, '-');
      $text = preg_replace('~-+~', '-', $text);
      $text = strtolower($text);
      if (empty($text)) {
        return 'n-a';
      }
      return $text;
    }

    public function searchBusiness($tags='',$location='everywhere',$date='anytime'){



      $results =
      DB::table('tags')
      ->select(
        'companies.id as companies_id',
        'companies.slot_step',
        'companies.company_name',
        'companies.cost_level',
        'companies.full_address as address',
        'companies.locality',
        'companies.route'
        )
        ->leftJoin('businesstype_tag','businesstype_tag.tag_id','=','tags.id')
        ->leftJoin('businesstypes','businesstype_tag.businesstype_id','=','businesstypes.id')
        ->leftJoin('companies','companies.businesstype_id','=','businesstypes.id');

        if($location!='everywhere'){
          $location = str_replace('-', ' ', trim($location));
          $prod = 1.1515 * 1.609344;
          $latitude = ''; #of location
          $longitude = ''; #of location
          $maxDistanceKm = 10;
          $geocode = $this->getGeoLocation($location);
          
          $latitude = $geocode['lat'];
          $longitude = $geocode['lng'];
          $results = $results
          ->addSelect(DB::raw('((ACOS(SIN('.$latitude.' * PI() / 180) * SIN(lat * PI() / 180) + COS('.$latitude.' * PI() / 180) * COS(lat * PI() / 180) * COS(('.$longitude.' - lng) * PI() / 180)) * 180 / PI()) * 60 * '.$prod.') as distance'))
          ->having('distance','<',$maxDistanceKm);
        }

        $results
        ->leftJoin('schedules','companies.id','=','schedules.company_id')
        ->leftJoin('schedulerows','schedules.id','=','schedulerows.schedule_id')
        ->leftJoin('time_dimension','schedulerows.day_of_the_week','=','time_dimension.day_of_the_week')
        ->addSelect('schedules.id as schedules_id')
        ->addSelect(DB::raw('(SELECT business_type_name FROM businesstypes WHERE businesstypes.id = companies.businesstype_id) as business_type_name'))
        ->where('schedulerows.status',1)
        ->addSelect('time_dimension.db_date as date')
        ->addSelect(DB::raw("(SELECT CONCAT('/storage/',id,'/',file_name) FROM media WHERE model_type LIKE 'App%Company' AND model_id = companies.id AND collection_name = 'company_gallery' ORDER BY order_column ASC LIMIT 1) as cover"))
        ->addSelect('schedulerows.available_minutes')
        ->addSelect('scheduleavailabilities.busy_minutes as occupied_minutes')
        ->addSelect(DB::raw("(SELECT service_price FROM services WHERE services.company_id = companies.id AND services.service_price > 0 ORDER BY services.service_price ASC LIMIT 1) as starting_price"))
        ->addSelect(
          DB::raw('(SELECT COALESCE(scheduleavailabilities.busy_minutes,0) as occupied_minutes) as occupied_minutes'))
          ->leftJoin('scheduleavailabilities',function($join){
            $join->on('scheduleavailabilities.day','=','time_dimension.db_date');
            $join->on('scheduleavailabilities.schedule_id','=','schedules.id');
          })
          ->havingRaw('(available_minutes - occupied_minutes) > 0'); #minimum of 0 minutes free


          if($date!='anytime'){
            $results
            ->where('time_dimension.db_date',$date);
          }else{
            $today = date('Y-m-d');
            $days = 7;
            $endDay = date('Y-m-d',strtotime('+'.$days.'days'));
            $results
            ->whereBetween('time_dimension.db_date',[$today,$endDay])
            ->orderBy('date','asc');
          }

          if($tags != ''){
            $results = $results
            ->where('tags.tag','like','%'.$tags.'%');
          }

          $results->groupBy(
            'companies_id',
            'available_minutes',
            'distance',
            'occupied_minutes',
            'date',
            'slot_step',
            'schedules_id',
            'company_name',
            'cost_level',
            'starting_price',
            'address',
            'locality',
            'route',
            'business_type_name'
          );

          $results = $results->get();


          /*
          companyUrl:function(company){
          var url = [];
          url.push(this.slugify(company.businesstype.business_type_name));
          url.push(this.slugify(company.company_name)+'-'+this.slugify(company.locality+' '+company.route));
          return 'it/'+url.join('/');
        },
        */

        $dayLabels = [
          date('Y-m-d')=>'Oggi',
          date('Y-m-d',strtotime('+1days'))=>'Domani'
        ];



        $return = [];
        if(!$results->isEmpty()){


          foreach($results as $result){
            $results_[$result->companies_id][] = $result;#group results by company id
          }

          foreach($results_ as $company_id => $companyResults){

            $result = $companyResults[0]; #foreach company just use the first result;

            $label = in_array($result->date,array_keys($dayLabels)) ? $dayLabels[$result->date] : date('d/m/Y',strtotime($result->date));

            $firstAvailableDate = [
              'label' => $label,
              'value' => $result->date
            ];

            $firstAvailableSlots = ['----','----','----'];

            $returnIndex = 'index_'.$result->companies_id;

            $return[$returnIndex]['id'] = $result->companies_id;
            $return[$returnIndex]['cover'] = $result->cover;
            $return[$returnIndex]['company_name'] = $result->company_name;
            $return[$returnIndex]['cost_level'] = $result->cost_level;
            $return[$returnIndex]['distance'] = number_format($result->distance,1);
            $return[$returnIndex]['starting_price'] = $result->starting_price;
            $return[$returnIndex]['address'] = $result->address;
            $return[$returnIndex]['first_available_date'] = $firstAvailableDate;
            $return[$returnIndex]['first_available_slots'] = $firstAvailableSlots;
            $url = [];
            $url[] = $this->slugify($result->business_type_name);
            $url[] = $this->slugify($result->company_name).'-'.$this->slugify($result->locality.' '.$result->route);
            $return[$returnIndex]['url'] = '/it/'.implode('/',$url);
          }
        }


        return $return;
      }

      /*
      {
      #cover:'https://ksr-ugc.imgix.net/assets/024/099/494/5ec5dbbae8f8ca176104e8165e0955d9_original.jpg?ixlib=rb-1.1.0&crop=faces&w=560&h=315&fit=crop&v=1550204884&auto=format&frame=1&q=92&s=77c5547d21dc1b4ea7b852a89ad853c6',
      #company_name:'Stefano Pavi',
      #cost_level:3,
      #distance:0.2,
      starting_price:'30.00',
      first_available_date:{
      label:'Domani',
      value:'2018-02-16'
    },
    first_available_slots:[
    {
    value:'15:30'
  },
  {
  value:'16:00'
},
{
value:'16:15'
}
],
address: 'Via Dei Serragli 17, 50124 Firenze'
},
*/


public function suggester($keyword=''){
  if($keyword!= ''){
    $results['tags'] = \App\Tag::withoutGlobalScopes()->where('tag','LIKE','%'.$keyword.'%')->get();
    $results['companies'] = \App\Company::with('businesstype')->withoutGlobalScopes()->where('company_name','LIKE','%'.$keyword.'%')->get();
    $results['brands'] = \App\Brand::with('businesstype')->withoutGlobalScopes()->where('brand_name','LIKE','%'.$keyword.'%')->get();
  }else{
    $results = [];
  }

  return $results;
}
}
