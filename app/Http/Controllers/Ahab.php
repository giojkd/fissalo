<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Businesstype;
use DB;

class Ahab extends Controller
{
    public function dispatchTraffic(Request $request, $tag)
    {
        $data = [];
        $tag = strtolower(trim($tag));
        $components = explode('-', $tag);
        $pos = strpos($tag, '-a-');
        if ($pos === false) {

            #IS CATEGORY
            $category = $components[0];
            unset($components[0]);
            $where = implode(' ', $components);
            
            /*
            echo 'category: '.$category;
            echo '<br>';
            echo 'where: '.$where;
            */

            $viewType = 'category';
            $data['keyword'] = ucfirst($category);
        } else {
            #IS BRAND
            $category = $components[0];
            unset($components[0]);
            $aux = explode('-a-', implode('-', $components));
            $brand = str_replace('-', ' ', $aux[0]);
            $where = str_replace('-', ' ', $aux[1]);

            /*
            echo 'category: '.$category;
            echo '<br>';
            echo 'brand: '.$brand;
            echo '<br>';
            echo 'where: '.$where;
            */

            $data['brand'] = $brand;
            $viewType = 'brand';
            $data['keyword'] = ucfirst($brand);
        }


        $data['viewType'] = $viewType;

        $data['where'] = $where;
        $data['when'] = ($request->get('il') != '') ? $request->get('il') : 'anytime';

        return view('results', $data);
    }

    public function serveBusiness($category, $businessFriendlyUrlName)
    {

        $checkTitle = str_replace('-',' ', $businessFriendlyUrlName);


        $check = DB::table('companies')
            ->select('id')
            ->selectRaw('(SELECT CONCAT(companies.company_name," ",companies.locality," ",companies.route)) as check_title')
            ->havingRaw('check_title LIKE "%' . $checkTitle . '%"')
            ->first();

        $data = [];
        $data['company_id'] = $check->id;


        return view('business', $data);
    }
}


#/parrucchiere-l-oreal-a-firenze BRAND
#/parrucchiere-via-dei-serragli-firenze CATEGORY
#/parrucchiere/stefano-pavi-via-dei-serragli-firenze BUSINESS
