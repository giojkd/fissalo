<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Company extends Controller
{

    public function loadBusinessTypes(){
      return \App\Businesstype::orderBy('business_type_name','ASC')->get();
    }

    public function getCompanyDetails($id)
    {
        $company = \App\Company::withoutGlobalScopes()->with(['photos','businesstype'])->findOrFail($id);
        $slides = [];
        foreach($company->photos as $photo){
            $slides[] = [
                'title' => '',
                'src' => '/storage/'.$photo->id.'/'.$photo->file_name
            ];
        }
        $return = [
            'company_name' => $company->company_name,
            'address' => $company->full_address,
            'distance' => 0.2,
            'category' => $company->businesstype->business_type_name,
            'slides' => $slides
        ];
        return $return;
    }


    public function getCompany($companyName,$companyId){


    	$company = \App\Company::findOrFail($companyId);
    	return $company;


    }

    public function getCompanies(){
      $companies = \App\Company::withoutGlobalScopes()->with(['photos','businesstype'])->get();

      $return = [];
      foreach($companies as $company){
        $cover = $company->photos[0];


        #NEEDS REAL VALUE

        $distance = rand(100,1000);
        $first_available_date = [
          'label' => 'Domani',
          'value' => '2018-02-16'
        ];
        $first_available_slots = [
          [
            'value' => '15:30'
          ],
          [
            'value' => '16:00'
          ],
          [
            'value' => '16:15'
          ]
        ];

        #NEEDS REAL VALUE

        $return[] = [
          'cover' => '/storage/'.$cover->id.'/'.$cover->file_name,
          'company_name' => $company->company_name,
          'business_type_name' => $company->businesstype->business_type_name,
          'cost_level' => $company->cost_level,
          'distance' => number_format($distance/1000,2),
          'first_available_date' => $first_available_date,
          'first_available_slots' => $first_available_slots
        ];



      }

      echo json_encode($return);
    }


    /*

    {
      cover:'https://duyt4h9nfnj50.cloudfront.net/resized/1544596763600-w640-e8.jpg',
      company_name:'Stefano Pavi',
      business_type_name:'Parrucchiere',
      cost_level:3,
      distance:0.2,
      first_available_date:{
        label:'Domani',
        value:'2018-02-16'
      },
      first_available_slots:[
        {
          value:'15:30'
        },
        {
          value:'16:00'
        },
        {
          value:'16:15'
        }
      ]
    }

    */

    public function getCompanyServices($companyId){
      $return = [];
      $services = \App\Service::with('servicecategory')->where('company_id',$companyId)->get();
      if(count($services)){
        foreach($services as $service){
          $category_id = $service->servicecategory->id;
          $return[$category_id]['category_id'] = $category_id;
          $return[$category_id]['category_label'] = $service->servicecategory->service_category_name;
          $return[$category_id]['category_services'][] = [
            'id' => $service->id,
            'title' => $service->service_name,
            'duration' => $service->service_duration,
            'description' => $service->service_description,
            'category_id' => $service->servicecategory_id,
            'price' => $service->service_price,
            'show' => true
          ];
        }
      }
      echo json_encode($return);
    }
}

/*

services:[
  {
    category_id:1,
    category_label:'Colore',
    category_services:[
      {
        id:1,
        title:'Taglio Donna',
        duration:45,
        description:'La colorazione aveda full spectrum immerge i capelli in una formula di derivazione naturale da 97% al 99%',
        category_id:1,
        price:30,
        show:true
      },
      {
        id:2,
        title:'Taglio Uomo',
        duration:35,
        description:'La colorazione aveda full spectrum immerge i capelli in una formula di derivazione naturale da 97% al 99%',
        category_id:1,
        price:25,
        show:true
      }
    ]
  }
],

*/
