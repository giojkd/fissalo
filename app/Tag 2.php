<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
     public function businesstypes()
    {
        return $this->belongsToMany('App\Businesstype');
    }
}
