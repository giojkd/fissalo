<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheduleavailability extends Model
{
    protected $fillable = ['schedule_id','day','busy_minutes'];
}
