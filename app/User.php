<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'name', 'surname', 'email', 'password','usertype_id','company_id'
  ];

  public function usertype()
  {
    return $this->belongsTo('App\Usertype');
  }

  public function company()
  {
    return $this->belongsTo('App\Company');
  }

  public function all_ok(){
    if($this->step_1_ok() && $this->step_2_ok() && $this->step_3_ok()){
      return true;
    }
    return false;
  }

  public function step_1_ok(){
    $company = $this->company;
    if(
      $company->company_description != '' &&
      $company->company_name != '' &&
      $company->company_description_short != '' &&
      #$company->company_telephone != '' &&
      #$company->company_email != '' &&
      $company->full_address != '' &&
      $company->slot_step != '' &&
      $company->cost_level != '')
      {
        return true;
      }
      return false;
    }

    public function step_2_ok(){
      $services = $this->company->services;
      if(!$services->isEmpty()){
        return true;
      }
      return false;
    }

    public function step_3_ok(){
      $schedules = $this->company->schedules;
      if(!$schedules->isEmpty()){
        return true;
      }
      return false;
    }

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
      'password', 'remember_token',
    ];
  }
