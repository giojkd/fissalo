<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Service extends Model
{
    public function schedules()
    {
        return $this->belongsToMany('App\Schedule');
    }

     public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function servicecategory(){
      return $this->belongsTo('App\Servicecategory');
    }

    public static function boot(){
      parent::boot();

      static::addGlobalScope('tenancy',function($builder){
        if(Auth::check()){
          $user = Auth::user();
          if($user->usertype_id != 1){
              $builder->where('company_id',$user->company_id);
          }

        }
      });
    }
}
