<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;

class Schedulerow extends Model
{
    //
    protected $fillable = ['day_of_the_week','schedule_id'];

    public static function boot() {
	    parent::boot();

	    static::created(function($schedule) {
	        Event::fire('App\Events\ReservationrowEvents', $schedule);
	    });

	    static::updated(function($schedule) {
	        Event::fire('App\Events\ReservationrowEvents', $schedule);
	    });



	}

}
