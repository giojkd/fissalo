<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Event;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Scopes\CompanyScope;
use Auth;

class Company extends Model implements HasMedia
{
  protected $fillable = ['businesstype_id','company_name','full_address'];

  use SoftDeletes;
  use HasMediaTrait;
  protected $dates = ['deleted_at'];
  protected $table = 'companies';



  public function photos(){
    return $this->morphMany('App\Media', 'model');
  }

  public function schedule(){
    return $this->hasMany('App\Schedule');
  }

  public function clients()
  {
    return $this->belongsToMany('App\Client');
  }

  public function reservationrow(){
    return $this->hasManyThrough('App\Reservationrow','App\Reservation');
  }

  public function businesstype()
  {
    return $this->belongsTo('App\Businesstype');
  }

  public function brands(){
    return $this->belongsToMany('App\Brand');
  }

  public function services(){
    return $this->hasMany('App\Service');
  }

  public function schedules(){
    return $this->hasMany('App\Schedule');
  }

  public static function boot() {

    parent::boot();
    static::addGlobalScope('tenancy',function($builder){
      if(Auth::check()){
        $user = Auth::user();
        if($user->usertype_id != 1){
          $builder->where('id',$user->company_id);
        }
      }
    });
  }
}
