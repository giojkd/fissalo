<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ClientScope;
use Auth;
use DB;

class Client extends Model
{
  #protected $fillable = ['client_name', 'client_surname', 'client_email','client_telephone'];
  protected $guarded = [];

  protected $dates = [
    'client_date_of_birth'
  ];

    protected $appends = ['full_name'];

  protected $casts = [
    'client_date_of_birth' => 'datetime:d-m-Y',
  ];

  public function getFullNameAttribute(){
    return $this->client_name.' '.$this->client_surname;
  }

  public function addresses()
  {
    return $this->hasMany('App\Address');
  }

  public function label(){
    return $this->client_name." ".$this->client_surname;
  }


  public function company()
  {
    return $this->belongsToMany('App\Company');
  }

  public function reservations(){
    return $this->hasMany('App\Reservation');
  }

  public static function boot() {

    parent::boot();

    static::addGlobalScope('tenancy',function($builder){
      if(Auth::check()){
        $user = Auth::user();
        if($user->usertype_id != 1){
          $builder
          ->select('clients.*')
          ->leftJoin('reservations','reservations.client_id','=','clients.id')
          ->where('reservations.company_id',$user->company_id)
          ->distinct();
          #$builder->addSelect(DB::raw('COUNT(reservations.id) as check_company FROM reservations WHERE reservations.id = clients.id AND reservations.company_id = '.$user->company_id))->having('check_company','>','0');
        }
      }
    });

  }

}
