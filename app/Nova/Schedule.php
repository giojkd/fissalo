<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Mgc\WeekSchedule\WeekSchedule;
use Silvanite\NovaFieldCheckboxes\Checkboxes;

use R64\NovaFields\JSON;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\BelongsTo;
use Service;

class Schedule extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Schedule';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    #public static $title = 'id';

     public function title()
    {
        return $this->schedule_name;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

     public static function label()
     {
         return 'Gestisci Agende';
     }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Name','schedule_name')
            ->sortable()
            ->rules('required', 'max:255'),
            WeekSchedule::make('Orario settimanale','week_schedule')->hideFromIndex()->hideFromDetail(),
            Checkboxes::make('Services','services_column')->options(\App\Service::pluck('service_name', 'id')->all())->withoutTypeCasting()->hideFromIndex(),
            BelongsToMany::make('Services'),
            BelongsTo::make('Company')->rules('required'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
