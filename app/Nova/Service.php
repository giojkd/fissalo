<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Number;

use Schedules;

class Service extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */

     public static function singularLabel()
     {
         return 'Nuovo Servizio';
     }

     public static function label()
     {
         return 'Gestisci i tuoi Servizi';
     }

    public static $model = 'App\Service';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    //public static $title = 'id';

     public function title()
    {
        return $this->service_name;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Nome','service_name')
            ->sortable()
            ->rules('required', 'max:255'),
            Currency::make('Prezzo di listino','service_retail_price')->rules('required'),
            Currency::make('Prezzo per Fissalo','service_price')->format('%.2n')->rules('required'),
            Number::make('Durata','service_duration')->min(5)->max(120)->step(1)->rules('required'),
            BelongsTo::make('Company')->rules('required'),
            BelongsTo::make('Servicecategory')->rules('required'),
            BelongsToMany::make('Schedules'),
            Textarea::make('Description','service_description'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
