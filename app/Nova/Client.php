<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Address;
class Client extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Client';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    //public static $title = 'id';

    public static function label()
    {
        return 'Elenco Clienti';
    }

    public function title()
    {
        return $this->client_name.' '.$this->client_surname;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Nome','client_name')->sortable()->rules('required', 'max:255'),
            Text::make('Cognome','client_surname')->sortable()->rules('required', 'max:255'),
            Text::make('Email','client_email')->sortable()->rules('required', 'max:255'),
            Text::make('Telefono','client_telephone')->sortable()->rules('required', 'max:255'),
            Date::make('Data di nascita','client_date_of_birth')->format('DD-MM-YYYY'),
            Boolean::make('Verificato','client_verified'),
            Select::make('Sesso','client_gender')->options([
                    'woman' => 'Donna',
                    'man' => 'Uomo'
                ])->displayUsingLabels(),
            HasMany::make('Addresses'),
            BelongsToMany::make('Company'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
