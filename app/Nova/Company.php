<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Boolean;
use EmilianoTisato\GoogleAutocomplete\AddressMetadata;
use EmilianoTisato\GoogleAutocomplete\GoogleAutocomplete;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Image;
use Mgc\WeekSchedule\WeekSchedule;
use R64\NovaFields\JSON;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Markdown;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Ebess\AdvancedNovaMediaLibrary\Fields\Files;
use Ebess\AdvancedNovaMediaLibrary\Fields\Media;
use Laravel\Nova\Fields\BelongsToMany;


class Company extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */

     public static function label()
     {
         return 'Gestisci Business';
     }
    public static $model = 'App\Company';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    //public static $title = 'id';

     public function title()
    {
        return $this->company_name;
    }
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
     protected function addressFields()
     {
         return $this->merge([
             Place::make('Address', 'address_line_1')->hideFromIndex(),
             Text::make('Address Line 2')->hideFromIndex(),
             Text::make('City')->hideFromIndex(),
             Text::make('State')->hideFromIndex(),
             Text::make('Postal Code')->hideFromIndex(),
             Country::make('Country')->hideFromIndex(),
             Text::make('Latitude')->hideFromIndex(),
             Text::make('Longitude')->hideFromIndex(),
         ]);
     }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Name','company_name')->sortable()->rules('required', 'max:255'),
            Text::make('Partita IVA','billing_info')->rules('required'),

            GoogleAutocomplete::make('Indirizzo','full_address')
            ->withValues(['latitude', 'longitude','locality','administrative_area_level_1','administrative_area_level_2','administrative_area_level_3','country','postal_code','route','street_number'])->countries('IT')->rules('required'),
            AddressMetadata::make('lat')->fromValue('latitude')->invisible()->hideFromIndex(),
            AddressMetadata::make('lng')->fromValue('longitude')->invisible()->hideFromIndex(),
            AddressMetadata::make('Locality','locality')->fromValue('locality')->invisible(),
            AddressMetadata::make('administrative_area_level_1')->fromValue('administrative_area_level_1')->invisible()->hideFromIndex(),
            AddressMetadata::make('administrative_area_level_2')->fromValue('administrative_area_level_2')->invisible()->hideFromIndex(),
            AddressMetadata::make('administrative_area_level_3')->fromValue('administrative_area_level_3')->invisible()->hideFromIndex(),
            AddressMetadata::make('country')->fromValue('country')->invisible()->hideFromIndex(),
            AddressMetadata::make('postal_code')->fromValue('postal_code')->invisible()->hideFromIndex(),
            AddressMetadata::make('route')->fromValue('route')->invisible()->hideFromIndex(),
            AddressMetadata::make('street_number')->fromValue('street_number')->invisible()->hideFromIndex(),


            BelongsTo::make('Tipo','businesstype','\App\Nova\Businesstype')->rules('required'),
            Textarea::make('Descrizione breve','company_description_short')->rules('required'),
            Textarea::make('Descrizione completa','company_description')->rules('required'),
            Image::make('Logo','company_logo')->disk('public')->prunable()->rules('required'),
            WeekSchedule::make('Orario settimanale','week_schedule')->hideFromIndex(),
            Boolean::make('Pagamento online','online_payment')->hideFromIndex(),
            Boolean::make('Accettazione automatica','auto_accept')->hideFromIndex(),
            Currency::make('Supplemento a domicilio','delivery_extra')->format('%.2n'),


            Images::make('Foto', 'company_gallery') // second parameter is the media collection name
            ->multiple() // enable upload of multiple images - also ordering
            ->fullSize() // full size column
            // validation rules for the collection of images
            ->rules('required')
            ->singleImageRules('dimensions:min_width=100'),

            BelongsToMany::make('Brands'),
            BelongsToMany::make('Clients'),
            Select::make('Livello di costo','cost_level')->options([
                '1' => '€',
                '2' => '€€',
                '3' => '€€€',
                '4' => '€€€€',
            ])->displayUsingLabels()->rules('required'),
            Select::make('Passo degli slot','slot_step')->options([
                '5' => '5m',
                '10' => '10m',
                '15' => '15m',
                '20' => '20m',
                '25' => '25m',
                '30' => '30m',
                '35' => '35m',
                '40' => '40m',
                '45' => '45m',
                '50' => '50m',
                '55' => '55m',
                '60' => '60m',
            ])->displayUsingLabels()->rules('required'),
            /*JSON::make('Week schedule', [
                Text::make('Name'),
                Boolean::make('Active'),
                Textarea::make('Description'),
            ], 'week_schedule'),*/
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
