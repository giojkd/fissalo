<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use EmilianoTisato\GoogleAutocomplete\AddressMetadata;
use EmilianoTisato\GoogleAutocomplete\GoogleAutocomplete;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Client;

class Address extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */

    public static function label()
    {
        return 'Indirizzi';
    }

    public static $model = 'App\Address';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    //public static $title = 'id';

    public function title()
    {
        return $this->full_address;
    }


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('Client'),
            GoogleAutocomplete::make('Indirizzo','full_address')->withValues(['latitude', 'longitude','locality','administrative_area_level_1','administrative_area_level_2','administrative_area_level_3','country','postal_code','route','street_number'])->countries('IT'),
            AddressMetadata::make('lat')->fromValue('latitude')->invisible()->hideFromIndex(),
            AddressMetadata::make('lng')->fromValue('longitude')->invisible()->hideFromIndex(),
            AddressMetadata::make('Locality','locality')->fromValue('locality')->invisible(),
            AddressMetadata::make('administrative_area_level_1')->fromValue('administrative_area_level_1')->invisible()->hideFromIndex(),
            AddressMetadata::make('administrative_area_level_2')->fromValue('administrative_area_level_2')->invisible()->hideFromIndex(),
            AddressMetadata::make('administrative_area_level_3')->fromValue('administrative_area_level_3')->invisible()->hideFromIndex(),
            AddressMetadata::make('country')->fromValue('country')->invisible()->hideFromIndex(),
            AddressMetadata::make('postal_code')->fromValue('postal_code')->invisible()->hideFromIndex(),
            AddressMetadata::make('route')->fromValue('route')->invisible()->hideFromIndex(),
            AddressMetadata::make('street_number')->fromValue('street_number')->invisible()->hideFromIndex(),
            Textarea::make('Note','address_notes')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
