<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
#use Illuminate\Database\Eloquent\SoftDeletes;

class Reservationrow extends Model
{

  #use SoftDeletes;

  protected $appends = ['begin_slot','slot_height'];


  protected $dates = [
    'reservation_begin','reservation_end'
  ];

  protected $casts = [
    #'reservation_begin' => 'datetime:d/m/Y H:i',
  ];

  protected $times = [
    'service_time_end','service_time'
  ];

  protected $fillable = ['service_time','service_time_end','service_id','service_price','schedule_id'];

  public function getBeginSlotAttribute(){
    return date('H:i',strtotime($this->reservation_begin));
  }


  public function getSlotHeightAttribute(){

    $company = $this->reservation->company;

    if(!is_null($company)){
      $slotStep = $company->slot_step;
    }else{
      $slotStep = 15;
    }

  return round((strtotime($this->reservation_end)-strtotime($this->reservation_begin))/60)/$slotStep*30;

  }


  public function getCountSlotsAttribute(){
    return $this->reservation->company->slot_step;
  }

  public function allReservations(){
    return $this->belongsTo('App\Reservation')->withoutGlobalScopes();
  }

  public function reservation()
  {
    return $this->belongsTo('App\Reservation');
  }

  public function service()
  {
    return $this->belongsTo('App\Service');
  }

  public function schedule(){
    return $this->belongsTo('App\Schedule');
  }


  public static function boot() {
    parent::boot();

    static::created(function($reservationrow) {
      Event::fire('App\Events\ReservationrowCreated', $reservationrow);
    });

    static::updated(function($reservationrow) {
      Event::fire('App\Events\ReservationrowUpdate', $reservationrow);
    });

    static::deleted(function($reservationrow) {
      Event::fire('App\Events\ReservationrowDeleted', $reservationrow);
    });

  }

}
